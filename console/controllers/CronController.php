<?php

namespace console\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\controllers\MyController;
use common\models\Customers;
use common\models\Logins;
use common\models\Sources;
use common\models\MentionsJobs;
use common\models\Words;
use common\models\Orders;
use common\models\Plans;
use frontend\models\Mentions;


//use Google\*;
//use Google\Service\*;
//Yii::import('application.components.Google.*');
//Yii::import('application.components.Google.Service.*');

define('JOBS_INTERVAL_MINUTES', 1);

class CronController extends \yii\console\Controller
{	
    public function actionTest() {
				\Yii::$app->mail->compose('expiredEmail', array(
																	'date'=>date('Y-m-d'),
																))
					->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['adminName']])
					->setTo('akrachev@gmail.com')
					->setSubject('eReport.bg | Abonament')
					->send();									
    }
    
	public function actionCron() {
		//if (date('G') == '1' && ((int)date('i') < 30)) {
			$this->alignSearchJobs();
			$this->disableExpired();
			$this->sendPaymentConfirmations();
		//}

		//if (date('G') == '8' && ((int)date('i') < 30)) {
			$this->sendMentionsNotification();
		//}
	}
	

	// aligns all search jobs to be subsequent at the early morning
	function actionPushSearchJobs() {
        $connection = \Yii::$app->db;
        
		$sources = Sources::findAll(['Active'=>1]);
		$iMinutes = JOBS_INTERVAL_MINUTES;
		foreach ($sources as $source) 
		{	
			$customers = Customers::findAll(['PlanActive'=>1]);	
			foreach ($customers as $customer) 
			{
                $words = Words::find(['Active' => 1, 'CustomerID' => $customer->CustomerID])
                            ->orderBy('WordID')
                            ->limit($customer->plan->WordsCount)->all();
                foreach ($words as $word) 
                {	
                    echo $word->WordText;
                    $cmd = $connection->createCommand()
                                ->insert('mentionsjobs', 
                                      [
                                          'CustomerID' => $customer->CustomerID,
                                          'SourceID' => $source->SourceID,
                                          'WordID' => $word->WordID,
                                          'WordGroupID' => $word->WordGroupID,
                                          'JobStatus' => 0, //JOB_Status_Pending,
                                          'JobStartTime' => new \yii\db\Expression('DATE_ADD( DATE( NOW( ) ) , INTERVAL '.$iMinutes.' MINUTE )'),
                                          'JobEndTime' => new \yii\db\Expression('NULL')
                                      ]
                                    );

                    $cmd->execute();				

                    $iMinutes += JOBS_INTERVAL_MINUTES;
                }                
			}	
		}	
	}


	// for each active customer
	// for each login - check last mentions schedule 
	function actionSendMentions() {
		$customers = Customers::findAll(['PlanActive'=>1]);	
		foreach ($customers as $customer) 
		{
			$words = Words::findAll(['CustomerID' => $customer->CustomerID, 'Active' => 1]);			
			$logins = Logins::findAll(['CustomerID' => $customer->CustomerID]);			
			foreach ($logins as $login) 
			{
                echo $login->Email;
				if ($login->MentionsSchedule == 0 || $login->MentionsSchedule == date('N')) //every day			
				{	
					$this->sendMentions($words, $login);
					
					$connection = \Yii::$app->db;		
					$connection	->createCommand()
						->update('logins', 
							  [
								'LastMentionsSchedule' => new \yii\db\Expression('NOW()'),
							  ],
							  'LoginID = ' . $login->LoginID
							)
					->execute();
				}	
			}			
		}
	}
	
	
	
	function sendMentions($words, $login) 
	{
		$mentions = [];
		$connection = \Yii::$app->db;
		
		foreach ($words as $word) {
			if ($login->MentionsNoEmail) {
				// nothing to do
			} 
			else 
			{
				$f = new \yii\i18n\Formatter();
				$sql = 'select *
						from mentions c
						where CustomerID = '.$login->CustomerID .'
						and WordID = '.$word->WordID .'
						and MentionDate > \'' . $f->asDate($login->LastMentionsSchedule, 'php:Y-m-d H:i:s') . '\'';

				$model = $connection->createCommand($sql);
				$mentions = $model->queryAll();
			}
			
            $email = empty($login->EmailForMentions) ? $login->Email : $login->EmailForMentions;
            
			if (sizeof($mentions) > 0) {
				$resultsTable = $this->renderPartial('results', array('word'=>$word->WordText, 'results'=>$mentions, 'word'=>$word->WordText), true);						
				\Yii::$app->mail->compose('resultsEmail', array('word'=>$word->WordText, 
																	'date'=>date('Y-m-d'),
																	'results'=>$resultsTable))
					->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['adminName']])
					->setTo($email)
					->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Alert for') . ' [' . $word->WordText . ']')
					->send();			
			}			
		} // foreach	
	}
	
	
	
	function sendPaymentConfirmations() {
		$sql = 'select *
				from invoices
				where Notified = 0';				
		$connection = \Yii::$app->db;

		$model = $connection->createCommand($sql);
		$invoices = $model->queryAll();
		foreach ($invoices as $invoice) {			
			// send confirmation email
			$order = Orders::findOne($invoice['OrderID']);
			$customer = Customers::findOne($order->CustomerID); // refresh
			$key = md5($customer->activkey);			
			$plan = Plans::findOne($order->PlanID);
			$f = new \yii\i18n\Formatter(); 
			$expiration = $f->asDate($customer->PlanExpirationTime);
			$logins = Logins::findAll(['CustomerID' => $customer['CustomerID']]);	
			foreach ($logins as $login) {				
				\Yii::$app->mail->compose('paymentEmail', array('id'=>$invoice['InvoiceID'],
																	'key'=>$key,
																	'plan'=>$plan->Name,
																	'expiration' => $expiration,
																))
					->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['adminName']])
					->setTo($login->Email)
					->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Payment confirmation'))
					->send();				
			}		
					
			$connection->createCommand()
						->update('invoices', 
								  [
									'Notified' => 1,
								  ],
								  'InvoiceID = ' . $invoice['InvoiceID']
								)
						->execute();					
		}		
	}
	
	
	
	function actionDisableExpired() {
		$sql = 'select *
				from customers c
				where Active = 1
				and Notified = 0
				and DATEDIFF( PlanExpirationTime, sysdate()) < 7';				
		$connection = \Yii::$app->db;

		$model = $connection->createCommand($sql);
		$customers = $model->queryAll();
		foreach ($customers as $customer) {			
			$logins = Logins::findAll(['CustomerID' => $customer['CustomerID']]);	
			foreach ($logins as $login) {				
				\Yii::$app->mail->compose('expireEmail', array(
																	'date'=>date('Y-m-d'),
																))
					->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['adminName']])
					->setTo($login->Email)
					->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Subscription notice'))
					->send();				
			}		
			
			$connection	->createCommand()
						->update('customers', 
								  ['Notified' => 1],
								  'CustomerID = ' . $customer['CustomerID']
								)
						->execute();
		}

		$sql = 'select *
				from customers c
				where Active = 1
				and Notified = 1
				and DATEDIFF( PlanExpirationTime, sysdate()) < 0';				
		$connection = \Yii::$app->db;

		$model = $connection->createCommand($sql);
		$customers = $model->queryAll();
		foreach ($customers as $customer) {			
			$logins = Logins::findAll(['CustomerID' => $customer['CustomerID']]);	
			foreach ($logins as $login) {				
				\Yii::$app->mail->compose('expiredEmail', array(
																	'date'=>date('Y-m-d'),
																))
					->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['adminName']])
					->setTo($login->Email)
					->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Subscription expired'))
					->send();									
			}		
			
			$connection	->createCommand()
						->update('customers', 
								  ['Notified' => 2, 'PlanActive' => 0],
								  'CustomerID = ' . $customer['CustomerID']
								)
						->execute();
		}		
	}			
}
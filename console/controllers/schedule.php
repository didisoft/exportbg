<?php
//$schedule = new omnilight\scheduling\Schedule();

$schedule->call(
        function(\yii\console\Application $app) {
            $app->runAction('cron/disable-expired');
        }
)->daily();
        
$schedule->call(
        function(\yii\console\Application $app) {
            $app->runAction('cron/push-search-jobs');
        }
)->dailyAt('05:00');
        
$schedule->call(
        function(\yii\console\Application $app) {
            $app->runAction('cron/send-mentions');
        }
)->hourly();

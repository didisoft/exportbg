<?php
return [
    'adminEmail' => 'office@ereport.bg',
	'adminName' => 'eReport.BG',
	'siteName' => 'eReport.bg',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
	
	// Invoice details
	'CompanyName' => 'ЕРЕПОРТ БЪЛГАРИЯ ЕООД',
	'BankName' => 'Първа Инвестициона Банка',
	'Address' => 'София, ул.Драговица 21, ап.120',
	'MOL' => 'Атанас Крачев',
	'IBAN'  => 'BG74FINV91501016421151',
	'EIK' => '202907062',
	'currency' => 'лв.',
	
	// Google Customsearch details
	// https://console.developers.google.com/project/forumcastle-504/apiui/credential
	'google_app_name' => "forumcastle-504",
	'google_api_key' => "AIzaSyCE_He037kHvNyanwat5I7PuwNAUrQvGBU",
	// https://cse.google.com/cse/setup/basic?cx=011316330735753128977:_kqlh5___w0
	'google_cse_cx_id' => '011316330735753128977:_kqlh5___w0',
	
	// Facebook
	// https://developers.facebook.com/tools/accesstoken/
	'facebook_ap_id' => '388009698050632',
	'facebook_secret' => 'e37ae3cd85955cd56fdace566de5ce64',
];

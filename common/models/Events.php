<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property integer $EventID
 * @property string $EventTime
 * @property integer $LoginID
 * @property string $Note
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EventTime'], 'safe'],
            [['CustomerID', 'Note', 'EventType'], 'required'],
            [['CustomerID', 'EventType'], 'integer'],
            [['Note'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'EventID' => 'Event ID',
            'EventTime' => 'Event Time',
            'LoginID' => 'Login ID',
            'Note' => 'Note',
        ];
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mentionsjobs".
 *
 * @property integer $JobID
 * @property integer $CustomerID
 * @property integer $JobStatus
 * @property integer $WordID
 * @property integer $SourceID
 * @property string $JobStartTime
 * @property string $JobEndTime
 * @property integer $WordGroupID
 * @property integer $MentionsCount
 */
class MentionsJobs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mentionsjobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerID', 'JobStatus', 'WordID', 'SourceID', 'JobStartTime', 'WordGroupID', 'MentionsCount'], 'required'],
            [['CustomerID', 'JobStatus', 'WordID', 'SourceID', 'WordGroupID', 'MentionsCount'], 'integer'],
            [['JobStartTime', 'JobEndTime'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JobID' => Yii::t('app', 'Job ID'),
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'JobStatus' => Yii::t('app', 'Job Status'),
            'WordID' => Yii::t('app', 'Word ID'),
            'SourceID' => Yii::t('app', 'Source ID'),
            'JobStartTime' => Yii::t('app', 'Job Start Time'),
            'JobEndTime' => Yii::t('app', 'Job End Time'),
            'WordGroupID' => Yii::t('app', 'Word Group ID'),
            'MentionsCount' => Yii::t('app', 'Mentions Count'),
        ];
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mentionspipe".
 *
 * @property integer $ID
 * @property integer $MentionID
 * @property integer $LoginID
 * @property string $PipeTime
 */
class MentionsPipe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mentionspipe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MentionID', 'LoginID', 'WordID'], 'required'],
            [['MentionID', 'LoginID', 'WordID'], 'integer'],
            [['PipeTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'MentionID' => 'Mention ID',
            'LoginID' => 'Login ID',
            'PipeTime' => 'Pipe Time',
        ];
    }
}

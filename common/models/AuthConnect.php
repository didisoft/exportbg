<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "authconnect".
 *
 * @property integer $AuthID
 * @property integer $LoginID
 * @property string $Email
 */
class AuthConnect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authconnect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LoginID', 'Email'], 'required'],
            [['LoginID'], 'integer'],
            [['Email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'AuthID' => 'Auth ID',
            'LoginID' => 'Login ID',
            'Email' => 'Email',
        ];
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sources".
 *
 * @property integer $SourceID
 * @property string $Name
 * @property integer $Active
 */
class Sources extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sources';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Active'], 'required'],
            [['Active'], 'integer'],
            [['Name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'SourceID' => Yii::t('app', 'Source ID'),
            'Name' => Yii::t('app', 'Name'),
            'Active' => Yii::t('app', 'Active'),
        ];
    }
}

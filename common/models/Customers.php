<?php

namespace common\models;

use Yii;
use \common\models\Plans;

/**
 * This is the model class for table "customers".
 *
 * @property integer $CustomerID
 * @property string $CompanyName
 * @property integer $PlanID
 * @property string $RegistrationTime
 * @property string $PlanExpirationTime
 * @property integer $Active
 * @property string $Discount
 * @property string $activkey
 * @property string $Address
 * @property string $MOL
 * @property string $EIK
 * @property string $IN_DDS
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['CompanyName', 'PlanID', 'RegistrationTime', 'PlanExpirationTime', 'Active', 'Discount', 'activkey', 'Address', 'MOL', 'EIK', 'IN_DDS'], 'required'],
            [['PlanID', 'Active', 'PlanActive', 'PaymentMethodID', 'Test'], 'integer'],
            [['RegistrationTime', 'PlanExpirationTime'], 'safe'],
            [['Discount', 'Notified'], 'number'],
            [['CompanyName', 'City', 'FastSpringRef', 'FastSpringUrl', 'activkey', 'MOL'], 'string', 'max' => 255],
            [['Address'], 'string', 'max' => 1000],
            [['EIK', 'IN_DDS'], 'string', 'max' => 12]
        ];
    }

	public function getPlan()
    {
      return \common\models\Plans::findOne($this->PlanID);
    }
   
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CustomerID' => 'Customer ID',
            'CompanyName' => Yii::t('app', 'Company Name'),
            'PlanID' => 'Plan ID',
            'RegistrationTime' => 'Registration Time',
            'PlanExpirationTime' => 'Plan Expiration Time',
            'Active' => 'Active',
            'Discount' => 'Discount',
            'activkey' => 'Activkey',
            'Test' => 'Test',
			'City' => Yii::t('app', 'City'),
            'Address' => Yii::t('app', 'Address'),
            'MOL' => Yii::t('app', 'MOL'),
            'EIK' => Yii::t('app', 'EIK'),
            'IN_DDS' => Yii::t('app', 'IN_DDS'),
        ];
    }
}

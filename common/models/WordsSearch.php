<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Words;

/**
 * WordsSearch represents the model behind the search form about `app\models\Words`.
 */
class WordsSearch extends Words
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WordID', 'CustomerID', 'WordGroupID', 'EnteredBy'], 'integer'],
            [['WordText', 'EnteredOn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $customerId)
    {
        $query = Words::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
			$this->CustomerID = $customerId;
            return $dataProvider;
        }

        $query->andFilterWhere([
            'WordID' => $this->WordID,
            'CustomerID' => $this->CustomerID,
            'WordGroupID' => $this->WordGroupID,
            'EnteredBy' => $this->EnteredBy,
            'EnteredOn' => $this->EnteredOn,
        ]);

        $query->andFilterWhere(['like', 'WordText', $this->WordText]);

        return $dataProvider;
    }
}

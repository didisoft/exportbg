<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "words".
 *
 * @property integer $WordID
 * @property integer $CustomerID
 * @property string $WordText
 * @property integer $WordGroupID
 * @property integer $EnteredBy
 * @property string $EnteredOn
 */
class Words extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'words';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerID', 'WordText', 'EnteredBy', 'EnteredOn'], 'required'],
            [['CustomerID', 'WordGroupID', 'EnteredBy'], 'integer'],
            [['EnteredOn', 'ExactMatch'], 'safe'],
			[['Lang'], 'string', 'max' => 10],
            [['WordText'], 'string', 'max' => 255],
			[['ExcludeTerms', 'OrTerms'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'WordID' => Yii::t('app', 'Word ID'),
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'WordText' => Yii::t('app', 'Word Text'),
            'WordGroupID' => Yii::t('app', 'Word Group ID'),
            'EnteredBy' => Yii::t('app', 'Entered By'),
            'EnteredOn' => Yii::t('app', 'Entered On'),
			'ExcludeTerms' => Yii::t('app', 'Words that don\'t have to exist in the results'),
			'OrTerms' => Yii::t('app', 'Alternative words to search for'),
			'ExactMatch' => Yii::t('app', 'Exact match'),
        ];
    }
}

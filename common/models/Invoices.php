<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoices".
 *
 * @property integer $InvoiceID
 * @property string $InvoiceDate
 * @property integer $CustomerID
 * @property integer $OrderID
 * @property string $OrderText
 * @property string $Amount
 * @property string $MOL
 * @property string $CompanyName
 * @property string $EIK
 * @property string $Address
 * @property integer $PaymentMethodID
 * @property string $IN_DDS
 */
class Invoices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InvoiceDate'], 'safe'],
			// not needed - Invoices are filled by our code only or manually
            //[['CustomerID', 'OrderID', 'OrderText', 'Amount', 'MOL', 'CompanyName', 'EIK', 'Address', 'PaymentMethodID', 'IN_DDS'], 'required'],
            [['CustomerID', 'OrderID', 'PaymentMethodID'], 'integer'],
            [['Amount'], 'number'],
            [['OrderText', 'MOL', 'CompanyName', 'Address'], 'string', 'max' => 255],
            [['EIK', 'IN_DDS'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'InvoiceID' => Yii::t('app', 'Invoice ID'),
            'InvoiceDate' => Yii::t('app', 'Invoice Date'),
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'OrderID' => Yii::t('app', 'Order ID'),
            'OrderText' => Yii::t('app', 'Order Text'),
            'Amount' => Yii::t('app', 'Amount'),
            'MOL' => Yii::t('app', 'Mol'),
            'CompanyName' => Yii::t('app', 'Company Name'),
            'EIK' => Yii::t('app', 'Eik'),
            'Address' => Yii::t('app', 'Address'),
            'PaymentMethodID' => Yii::t('app', 'Payment Type ID'),
            'IN_DDS' => Yii::t('app', 'In  Dds'),
        ];
    }
}

<?php
namespace common\controllers;

use Yii;
use yii\web\Controller;
use yii\log\Logger;

use common\models\Orders;
use common\models\Customers;
use common\models\Plans;
use common\models\Logins;
use common\models\Events;

define('JOB_Status_Pending', '0');
define('JOB_Status_Started', '1');
define('JOB_Status_Finished', '2');
define('JOB_Status_Notified', '3');
define('JOB_Status_Error', '9');

define('JOB_ID_Google', '1');
define('JOB_ID_Facebook', '2');
define('JOB_ID_Twitter', '3');
define('JOB_ID_Bing', '4');

// OrderStatusID
define ('ORDER_PENDING', '0');
define ('ORDER_PAID', '1');
define ('ORDER_CANCELED', '-1');

// PaymentMethodID
define ('PAYMENT_METHOD_BANK', '1');
define ('PAYMENT_METHOD_EPAY', '2');
define ('PAYMENT_METHOD_FASTSPRING', '4');
define ('PAYMENT_METHOD_CLICKBANK', '5');
define ('PAYMENT_METHOD_MANUAL', '9');

// Events.EventType
define ('EVENT_LOGIN', '1');
define ('EVENT_REGISTER', '2');
define ('EVENT_ADD_WORD', '3');
define ('EVENT_CHANGE_WORD', '4');
define ('EVENT_DELETE_WORD', '5');
define ('EVENT_ORDER_START', '6');
define ('EVENT_ORDER_END', '7');
define ('EVENT_SEARCH', '9');

define ('MAX_PLAN_ID', '4');
define ('SYS_IBAN', 'BG74FINV91501016421151');

/**
 * Base controller
 */
class MyController extends Controller
{
	private function eventTypeText($eventType) {
		if (EVENT_LOGIN == $eventType) return 'LOGIN';
		if (EVENT_REGISTER == $eventType) return 'REGISTER';
		if (EVENT_ADD_WORD == $eventType) return 'ADD_WORD';
		if (EVENT_CHANGE_WORD == $eventType) return 'CHANGE_WORD';
		if (EVENT_DELETE_WORD == $eventType) return 'DELETE_WORD';
		if (EVENT_ORDER_START == $eventType) return 'ORDER_START';
		if (EVENT_ORDER_END == $eventType) return 'ORDER_END';
		if (EVENT_SEARCH == $eventType) return 'SEARCH';
		
		return 'UNKNOWN '.$eventType;
	}
	
	protected function event($customerId, $eventType, $note='')
	{
        $audit = new \common\models\AuditTrail();
        $audit->app_name = Yii::$app->name;
        $audit->method_name = Yii::$app->request->url;
        $audit->tenant_id = $customerId;
        $audit->event_date = new \yii\db\Expression('NOW()');
        $audit->params = '';
        $audit->result = '1';
        $audit->client_ip = Yii::$app->request->userIP;
        $audit->exception_info = '';
        $audit->exception_detail = '';
        $audit->save(false);
	}

	protected function trace($var) {
		return $this->debug($var, Logger::LEVEL_TRACE);		
	}
	
	protected function debug($var, $level) {
	  if (is_array($var)) {
		  foreach ($var as $k => $v) 
		  {
			Yii::getLogger()->log(sprintf('%s = %s', $k, $v),
					$level,
					'app');	
		  }
	  } else {
			Yii::getLogger()->log(sprintf('%s', $var),
					$level,
					'app');	  
	  }
	}	

	protected function error($message) {
		$this->debug($message, Logger::LEVEL_ERROR);		
	}			

	
	
    protected function recordPaidOrder($id, $details = '')
    {
        if (($model = Orders::findOne($id)) !== null) {
            $order = $model;
        } else {
            throw new NotFoundHttpException('Order not found, id: ' . $id);
        }
		$customer = Customers::findOne($order->CustomerID);

		if ($order->OrderStatusID == ORDER_PAID) {
			return;
		}
		
		$connection = \Yii::$app->db;		
		$transaction = $connection->beginTransaction();
		try {
			$connection	->createCommand()
						->update('orders', 
								  [
									'OrderStatusID' => ORDER_PAID,
									'Details' => $details,
									'PayDate' => new \yii\db\Expression('NOW()'),
								  ],
								  'OrderID = ' . $order->OrderID
								)
						->execute();

			// plan extending
            $connection	->createCommand()
                        ->update('customers', 
                                  [
                                    'Balance' => $customer->Balance - $order->BalanceMinus,
                                    'PlanID' => $order->PlanID,
                                    'PlanExpirationTime' => new \yii\db\Expression('date_add(PlanExpirationTime, interval '.$order->Period.' month)'),
                                    'PlanActive' => 1,
                                    'Notified' => 0,
                                  ],
                                  'PlanExpirationTime > sysdate() AND CustomerID = ' . $order->CustomerID
                                )
                        ->execute();
            $connection	->createCommand()
                        ->update('customers', 
                                  [
                                    'Balance' => $customer->Balance - $order->BalanceMinus,
                                    'PlanID' => $order->PlanID,
                                    'PlanExpirationTime' => new \yii\db\Expression('date_add(sysdate(), interval '.$order->Period.' month)'),
                                    'PlanActive' => 1,
                                    'Notified' => 0,
                                  ],
                                  'PlanExpirationTime <= sysdate() AND CustomerID = ' . $order->CustomerID
                                )
                        ->execute();
						
			$transaction->commit();						
		} catch(Exception $e) {
			$this->error($e->getTraceAsString());
			$transaction->rollback();
		}
		
		return;
    }	
    
    
    // $client is yiiauth/BaseClient
    function getAttrEmail($client) {
        $attributes = $client->getUserAttributes();
        if ($client->getName() == 'google') {
            $email = $attributes['emails'][0]['value'];
        } else if ($client->getName() == 'facebook') {
            $email = $attributes['email'];
        }        
        return $email;
    }    
    
    
    
	function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}	    
}

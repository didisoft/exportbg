<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustomersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'CustomerID') ?>

    <?= $form->field($model, 'CompanyName') ?>

    <?= $form->field($model, 'PlanID') ?>

    <?= $form->field($model, 'RegistrationTime') ?>

    <?= $form->field($model, 'PlanExpirationTime') ?>

    <?php // echo $form->field($model, 'Active') ?>

    <?php // echo $form->field($model, 'Discount') ?>

    <?php // echo $form->field($model, 'activkey') ?>

    <?php // echo $form->field($model, 'Address') ?>

    <?php // echo $form->field($model, 'MOL') ?>

    <?php // echo $form->field($model, 'EIK') ?>

    <?php // echo $form->field($model, 'IN_DDS') ?>

    <?php // echo $form->field($model, 'City') ?>

    <?php // echo $form->field($model, 'Balance') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

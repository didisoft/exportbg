<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use common\models\Plans;

use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CompanyName')->textInput(['maxlength' => 255]) ?>

<?php 
//use app\models\Country;
$countries = Plans::find()->all();

//use yii\helpers\ArrayHelper;
$listData = ArrayHelper::map($countries,'PlanID','Name');

echo $form->field($model, 'PlanID')->dropDownList(
								$listData, 
								['prompt'=>'Select...']
								);
?>	

    <?= $form->field($model, 'RegistrationTime')->textInput() ?>

    <label>PlanExpirationTime</label>

	<?php echo DatePicker::widget([
		'model' => $model, 
		'attribute' => 'PlanExpirationTime',
		'type' => DatePicker::TYPE_INPUT,
		'pluginOptions' => [
			'autoclose'=>true,
			'format' => 'yyyy-mm-dd'
		],
		'options' => [],
	]);
	?>
	
    <?= $form->field($model, 'Active')->dropDownList(
								['0' => 'No', '1' => 'Yes'] )  ?>

    <?= $form->field($model, 'Discount')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'activkey')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Address')->textInput(['maxlength' => 1000]) ?>

    <?= $form->field($model, 'MOL')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'EIK')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'IN_DDS')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'City')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

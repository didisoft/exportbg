<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
?>
<h1>google/index</h1>

<div class="settings">

    <?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'q') ?>
		    
        <div class="form-group">
		
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- settings -->

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Plans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'WordsCount')->textInput() ?>

    <?= $form->field($model, 'LoginsCount')->textInput() ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'Price')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'Price3m')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'Price6m')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'Price12m')->textInput(['maxlength' => 10]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

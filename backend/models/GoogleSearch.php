<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * OrdersSearch represents the model behind the search form about `common\models\Orders`.
 */
class GoogleSearch extends Model
{

    public $q;
	public $days;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['q', 'days'], 'safe'],
        ];
    }
	
	public function attributeLabels()
    {
        return [
            'q' => 'q: search phrase',
			'days' => 'days back',
        ];
    }		
}

<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OrderID', 'CustomerID', 'PlanID', 'Period', 'PaymentMethodID', 'OrderStatusID'], 'integer'],
            [['OrderDate', 'MOL', 'EIK', 'Address', 'CompanyName', 'IN_DDS'], 'safe'],
            [['Amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */	
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'OrderID' => $this->OrderID,
            'OrderDate' => $this->OrderDate,
            'CustomerID' => $this->CustomerID,
            'PlanID' => $this->PlanID,
            'Period' => $this->Period,
            'Amount' => $this->Amount,
            'PaymentMethodID' => $this->PaymentMethodID,
            'OrderStatusID' => $this->OrderStatusID,
        ]);

        $query->andFilterWhere(['like', 'MOL', $this->MOL])
            ->andFilterWhere(['like', 'EIK', $this->EIK])
            ->andFilterWhere(['like', 'Address', $this->Address])
            ->andFilterWhere(['like', 'CompanyName', $this->CompanyName])
            ->andFilterWhere(['like', 'IN_DDS', $this->IN_DDS]);

        return $dataProvider;
    }
}

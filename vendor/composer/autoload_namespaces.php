<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'cebe\\gravatar\\' => array($vendorDir . '/cebe/yii2-gravatar'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'HybridLogic' => array($vendorDir . '/hybridlogic/classifier/src'),
    'HTMLPurifier' => array($vendorDir . '/ezyang/htmlpurifier/library'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
);

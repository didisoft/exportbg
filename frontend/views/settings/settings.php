<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Account');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $this->title ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

    <?php $form = ActiveForm::begin(); ?>
              <div class="box-body">

        <?= $form->field($model, 'FirstName') ?>
        <?= $form->field($model, 'LastName') ?>
		<?= $form->field($model, 'Email') ?>
    
        <div class="form-group">
		
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- settings -->

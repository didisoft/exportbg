<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Парола';
$this->params['breadcrumbs'][] = [
            'label' => 'Настройки',
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<div class="settings">

    <?php $form = ActiveForm::begin(); ?>

		<div class="form-group">
		<label style="width: 200px;" for="old_password">Текуща парола</label>
        <?= Html::passwordInput('old_password') ?>
		</div>
		
		<div class="form-group">
		<label style="width: 200px;" for="new_password">Нова парола</label>
		<?= Html::passwordInput('new_password') ?>
		</div>
		
		<div class="form-group">
		<label style="width: 200px;" for="repeat_password">Повторете Новата парола</label>
		<?= Html::passwordInput('repeat_password') ?>
		</div>
    
        <div class="form-group">
		
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- settings -->

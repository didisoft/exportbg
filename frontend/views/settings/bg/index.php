<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\editable\Editable;
use kartik\popover\PopoverX;

$this->title = Yii::t('app','Settings');
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<span style="font-size: 1.4em; font-weight: bold;"><?= Yii::t('app', 'MentionsSchedule') ?></span>
<?php
	echo Editable::widget([
		'model'=>$model, 
		'inputType' => 'dropDownList',
		'displayValueConfig' => [0 => 'Всеки ден', 
							1 => Yii::t('app', 'Monday'), 
							2 => Yii::t('app', 'Tuesday'),
							3 => Yii::t('app', 'Wednesday'),
							4 => Yii::t('app', 'Thursday'),
							5 => Yii::t('app', 'Friday'),
							6 => Yii::t('app', 'Saturday'),
							7 => Yii::t('app', 'Sundey')],
		'data' => [0 => Yii::t('app', 'Everyday'), 
					1 => Yii::t('app', 'Monday'), 
					2 => Yii::t('app', 'Tuesday'),
					3 => Yii::t('app', 'Wednesday'),
					4 => Yii::t('app', 'Thursday'),
					5 => Yii::t('app', 'Friday'),
					6 => Yii::t('app', 'Saturday'),
					7 => Yii::t('app', 'Sundey')],
		'attribute' => 'MentionsSchedule',
		'size' => 'md',
		'format' => 'link',
		'type' => 'post',
		'editableValueOptions'=>['class'=>'well well-sm'],
		'formOptions' => ['method'=>'post', 'action'=> ['settings/editmailpreferences']]
	]);
?>	
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => 'В кои дни желаете да получавате Email с нови резултати',
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>	

<?= Yii::t('app', 'Mentions by Email') ?>&nbsp;
<?php
	echo Editable::widget([
		'model'=>$model, 
		'inputType' => 'dropDownList',
		'displayValueConfig' => [0 => 'Желая', 
							1 => Yii::t('app', 'Не желая')], 
		'data' => [0 => 'Желая', 
							1 => Yii::t('app', 'Не желая')], 
		'attribute' => 'MentionsNoEmail',
		'size' => 'md',
		'format' => 'link',
		'type' => 'post',
		'editableValueOptions'=>['class'=>'well well-sm'],
		'formOptions' => ['method'=>'post', 'action'=> ['settings/editmailpreferences']]
	]);
?>	
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => 'Ако не желаете да получавате Email известия при нови резултати, изберете - Не желая',
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>	
<br/>

<h2></h2>
<div class="settings">
		<span style="font-size: 1.4em; font-weight: bold;">Лични данни</span>
        <?= $model->Email ?>
    
		<?= Html::a(Yii::t('app', 'Edit'), ['settings/settings' ], ['class' => 'btn btn-info']) ?>
</div> 

<h2></h2>
<div class="settings">
	<span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'Password') ?></span>
	<?= Html::a(Yii::t('app', 'Edit'), ['settings/password' ], ['class' => 'btn btn-info']) ?>
</div> 

<h2></h2>
<div class="settings">
	<span style="font-size: 1.4em; font-weight: bold;">Данни за фактура</span>

        <?= $customer->CompanyName ?>
		<?= Yii::t('app', 'EIK'); ?>
        <?= $customer->EIK ?>
    
		<?= Html::a(Yii::t('app', 'Edit'), ['settings/company' ], ['class' => 'btn btn-info']) ?>
</div> 

<?php if ($model->IsAdmin && $plan->LoginsCount > 1) { ?>
<h2></h2>
<div class="settings">
	<span style="font-size: 1.4em; font-weight: bold;">Допълнителни потребители</span>
	<?php echo Html::a(Yii::t('app', 'Edit'), ['logins/index' ], ['class' => 'btn btn-info']) ?>
</div> 	
<?php } ?>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Лични данни';
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<div class="settings">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'FirstName') ?>
        <?= $form->field($model, 'LastName') ?>
		<?= $form->field($model, 'Email') ?>
    
        <div class="form-group">
		
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- settings -->

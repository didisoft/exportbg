<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Company details');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Settings'),
            'url' => ['settings/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<div class="settings">

    <?php $form = ActiveForm::begin(['action' => ['settings/company']]); ?>
	
	<?php echo $form->field($model, 'CompanyName')->textInput() ?>
	<?php echo $form->field($model, 'EIK')->textInput() ?>
	<?php echo $form->field($model, 'MOL')->textInput() ?>
	<?php echo $form->field($model, 'Address')->textInput() ?>
	<?php echo $form->field($model, 'IN_DDS')->textInput() ?>
    
	<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('app', 'Cancel'), ['settings/index']) ?>
	
    <?php ActiveForm::end(); ?>

</div><!-- settings -->

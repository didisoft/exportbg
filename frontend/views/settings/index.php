<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\authclient\widgets\AuthChoice;	

use kartik\editable\Editable;
use kartik\popover\PopoverX;

$this->title = Yii::t('app','Settings');
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<style>
.auth-icon.facebook {
    color: #ffffff;
    background-color: #395697;
    line-height: 32px;
    padding: 0 40px;
    margin-left: 10px;
    width: 136px;
    float: left;
}
.auth-icon.google {
    color: #ffffff;
    background-color: #e0492f;
    line-height: 32px;
    padding: 0 40px;
    width: 136px;
    float: left;
}
    
</style>
<div class="row">    
    <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-user-o"></i> <?= Yii::t('app', 'Account') ?></h3>
          </div><!-- /.box-header -->
          <div class="box-body">
              <p>
                <?= Html::a(Yii::t('app', 'Edit'), ['settings/settings' ], ['class' => 'btn btn-info']) ?>
                <span style="font-size: 1.4em; font-weight: bold;"><?= Yii::t('app', 'Account') ?></span>
               </p>
               <p>
                <?= Html::a(Yii::t('app', 'Edit'), ['settings/password' ], ['class' => 'btn btn-info']) ?>
                <span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'Password') ?></span>
               </p>
                <?php if ($model->IsAdmin && $plan->LoginsCount > 1) { ?>
                <p>
                <div class="settings">
                    <span style="font-size: 1.4em; font-weight: bold;"><?php echo Yii::t('app', 'Additional Accounts') ?></span>
                    <?php echo Html::a(Yii::t('app', 'Edit'), ['logins/index' ], ['class' => 'btn btn-info']) ?>
                </div> 	
                </p>
                <?php } ?>
          </div>
          <div class="box-footer">
              <div class="info-box-text" style="float: left">  
                    <?= Yii::t('app', 'Connect my account with &nbsp;&nbsp;&nbsp;') ?>&nbsp;  
              </div>
            <?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['settings/connect']]); ?>
            <?php foreach ($authAuthChoice->getClients() as $client) {
                    if ($client->getName() == 'google') {
                        $client->returnUrl = Yii::$app->params['siteUrl'] . '/settings/connect?authclient=google';
                    }
                    echo $authAuthChoice->clientLink($client, Html::tag('span', $client->getTitle(), ['class' => 'auth-icon ' . $client->getName()]));
                  } ?>
            <?php AuthChoice::end(); ?>                
          </div><!-- box-footer -->          
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-bell"></i><?= Yii::t('app', 'Notifications') ?></h3>
          </div><!-- /.box-header -->
          
          <div class="box-body">
              

    <?= Yii::t('app', 'Email for receiving mentions') ?>
    <?php
        $mails = [Yii::$app->user->identity->Email];
        $socials = common\models\AuthConnect::findAll(['LoginID' => Yii::$app->user->identity->LoginID]);
        foreach ($socials as $social) {
            $mails[] = $social->Email;
        }
    
        echo Editable::widget([
            'model'=>$model, 
            'inputType' => 'dropDownList',
            'displayValueConfig' => $mails,
            'data' => $mails,
            'attribute' => 'EmailForMentions',
            'valueIfNull' => $mails[0],
            'size' => 'md',
            'format' => 'link',
            'type' => 'post',
            'editableValueOptions'=>['class'=>'well well-sm'],
            'formOptions' => ['method'=>'post', 'action'=> ['settings/editmailpreferences']]
        ]);
    ?>	

        <br/>
                      
<?= Yii::t('app', 'Mentions by Email') ?>&nbsp;
<?php
	echo Editable::widget([
		'model'=>$model, 
		'inputType' => 'dropDownList',
		'displayValueConfig' => [0 => Yii::t('app', 'Yes'), 
							1 => Yii::t('app', 'No')], 
		'data' => [0 => Yii::t('app', 'Yes'), 
							1 => Yii::t('app', 'No')], 
		'attribute' => 'MentionsNoEmail',
		'size' => 'md',
		'format' => 'link',
		'type' => 'post',
		'editableValueOptions'=>['class'=>'well well-sm'],
		'formOptions' => ['method'=>'post', 'action'=> ['settings/editmailpreferences']]
	]);
?>	
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => Yii::t('app', 'Would you like mentiones notifications by Email'),
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>	
          </div>
          <div class="box-footer">
              <?= Yii::t('app', 'You can customize where to receive mention alerts.') ?>
          </div><!-- box-footer -->
        </div>        
    </div>        
</div>



<?php if (Yii::$app->language == 'bg') { ?>
<div class="row">    
    <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-user-o"></i> <?= Yii::t('app', 'Account') ?></h3>
          </div><!-- /.box-header -->
          <div class="box-body">
        <span style="font-size: 1.4em; font-weight: bold;">Данни за фактура</span>

        <?= $customer->CompanyName ?>
		<?= Yii::t('app', 'EIK'); ?>
        <?= $customer->EIK ?>
    
		<?= Html::a(Yii::t('app', 'Edit'), ['settings/company' ], ['class' => 'btn btn-info']) ?>
        </div> 
        </div>
    </div>
</div>
          
<?php } ?>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Logins */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Logins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'New record');
?>
<div class="logins-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

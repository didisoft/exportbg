<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LoginsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Logins');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logins-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
	<?php 
	if ($plan->LoginsCount > $dataProvider->count+1 /* counting self */) { ?>		
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
		<?php
			echo PopoverX::widget([
				'header' => 'Информация',
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => 'Тук можете да добавите допълнителни потребители с достъп до системата.',
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info'],
			]);	
		?>					
	<?php } else { ?>	
		Достигнахте максимума от <?= $plan['LoginsCount'] ?> регистрирани потребители за Абонаментния план.
	<?php } ?>			
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'LoginID',
            //'CustomerID',
            'FirstName',            
			'LastName',
			'Email:email',			
            //'IsAdmin',
            // 
            // 'Username',
            // 'Password',
            // 'password_reset_token',
            // 'created_at',
            // 'updated_at',
            // 'auth_key',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>

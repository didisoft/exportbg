<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logins-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Email')->textInput(['maxlength' => true])->hint('E-mail за достъп до системата') ?>
    <?= $form->field($model, 'Password')->textInput(['maxlength' => true])->hint('Парола за достъп до системата') ?>

    <?= $form->field($model, 'IsAdmin')->checkbox()->hint('Дава права за редакция на потребители.') ?>

    <?= $form->field($model, 'FirstName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LastName')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

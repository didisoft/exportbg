<?php
//use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Invoice');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Subscription'),
            'url' => ['billing/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
  
?>
<style>

.table th { background-color: #f5f5f5; }
.table td { border: solid 1px;}
.text-right { text-align: right};
.table-bordered { border: solid 1px;}
</style>
<table>
      <tr>
        <td>
          <h1>
            eReport.bg
          </h1>
        </td>
        <td>
          <h1><?php echo Yii::t('app', 'Invoice') ?> #<?php echo $invoice->InvoiceID ?> (Оригинал)</h1>
          <h1><small>Дата: <?php $f = new \yii\i18n\Formatter(); echo $f->asDate($invoice->InvoiceDate); ?></small></h1>
        </td>
      </tr>
      <tr>
        <td>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>Получател : <?php echo $invoice->CompanyName ?></h4>
            </div>
            <div class="panel-body">
              <p>
				Ид. номер: <?php echo $invoice->EIK ?><br/>
				Адрес: <?php echo $invoice->City ?>, <?php echo $invoice->Address ?><br/>
				МОЛ: <?php echo $invoice->MOL ?>
              </p>
            </div>
          </div>
        </td>
        <td>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>Издател: <?php echo \Yii::$app->params['CompanyName'] ?></h4>
            </div>
            <div class="panel-body">
              <p>
				Ид. номер: <?php echo \Yii::$app->params['EIK'] ?><br/>
                Адрес: <?php echo \Yii::$app->params['Address'] ?><br/>
				МОЛ: <?php echo \Yii::$app->params['MOL'] ?>
              </p>
            </div>
          </div>
        </td>
      </tr>
	  <tr>
		<td colspan="2" align="right">
      <!-- / end client details section -->
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>
              <h4>Стоки/услуги</h4>
            </th>
            <th>
              <h4>Количество</h4>
            </th>
            <th>
              <h4>Ед. цена</h4>
            </th>
            <th>
              <h4>Стойност</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $invoice->OrderText ?></td>
            <td class="text-right">1</td>
            <td class="text-right"><?php echo $invoice->Amount . \Yii::$app->params['currency']?></td>
            <td class="text-right"><?php echo $invoice->Amount . \Yii::$app->params['currency']?></td>
          </tr>
        </tbody>
      </table>
	</td>
	</tr>
	<tr>
    <td align="right" colspan="2">	
      <table>
        <tr>
          <td>
            <strong>
            Данъчна основа : <br>
            Ставка 0% ДДС : <br>
            Общо : <br>
            </strong>
          </td>
          <td>
          <?php echo $invoice->Amount . \Yii::$app->params['currency']?> <br>
          0 <?= \Yii::$app->params['currency'] ?><br>
          <?php echo $invoice->Amount . \Yii::$app->params['currency']?> <br>
          </td>
        </tr>
      </table>
	</td>
 </tr>
 <tr>
    <td>	
	  Основание на сделката: чл.113, ал.9 от ЗДДС
	</td>
 </tr>
 <tr>
    <td>	
      <table>
        <tr>
          <td>
            <div class="panel-heading">
              <h4>Плащане по сметка</h4>
            </div>
            <table>
              <tr><td>Получател: </td><td><?php echo \Yii::$app->params['CompanyName'] ?></td></tr>
              <tr><td>Банка: </td><td><?php echo \Yii::$app->params['BankName'] ?></td></tr>
              <tr><td>IBAN : </td><td><?php echo \Yii::$app->params['IBAN'] ?></td></tr>
			  <tr><td>Банков код: </td><td>FINVBGS</td></tr>
            </table>
          </td>
        </tr>
	</table>
	</td>
	<td valign="top" align="right">
              <div class="panel-heading">
                <h4>Съставил: Атанас Крачев</h4>
              </div>
              <div class="panel-body">
                <p>Подпис на съставителя: АСК11
                </p>
                <h4></h4>
              </div>
	</td>
  </tr>
 </table>
	  
	<small>Съгласно чл.7, ал 1 и чл. 8 от Закона за счетоводството, чл.114 от ЗДДС и чл.78 от ППЗДДС печатът не е задължителен реквизит на
фактурата, а подписите са заменени с идентификационни шифри<small>
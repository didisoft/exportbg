<?php
//use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Invoice');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Subscription'),
            'url' => ['billing/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
  ?>

<?php echo Html::a(Yii::t('app','Print'), 
				['billing/invoice-print', 'id' => $invoice->InvoiceID],
				['target' => '_blank']) ?>

<div class="container">
      <div class="row">
        <div class="col-xs-6">
          <h1>
            eReport.bg
          </h1>
        </div>
        <div class="col-xs-6 text-right">
          <h1><?php echo Yii::t('app', 'Invoice') ?> #<?php echo $invoice->InvoiceID ?> (Оригинал)</h1>
          <h1><small>Дата: <?php $f = new \yii\i18n\Formatter(); echo $f->asDate($invoice->InvoiceDate); ?></small></h1>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>Получател : <?php echo $invoice->CompanyName ?></h4>
            </div>
            <div class="panel-body">
              <p>
				Ид. номер: <?php echo $invoice->EIK ?><br/>
				Адрес: <?php echo $invoice->City ?>, <?php echo $invoice->Address ?><br/>
				МОЛ: <?php echo $invoice->MOL ?>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2 text-right">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>Издател: <?php echo \Yii::$app->params['CompanyName'] ?></h4>
            </div>
            <div class="panel-body">
              <p>
				Ид. номер: <?php echo \Yii::$app->params['EIK'] ?><br/>
                Адрес: <?php echo \Yii::$app->params['Address'] ?><br/>
				МОЛ: <?php echo \Yii::$app->params['MOL'] ?>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- / end client details section -->
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>
              <h4>Стоки/услуги</h4>
            </th>
            <th>
              <h4>Количество</h4>
            </th>
            <th>
              <h4>Ед. цена</h4>
            </th>
            <th>
              <h4>Стойност</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $invoice->OrderText ?></td>
            <td class="text-right">1</td>
            <td class="text-right"><?php echo $invoice->Amount . \Yii::$app->params['currency']?></td>
            <td class="text-right"><?php echo $invoice->Amount . \Yii::$app->params['currency']?></td>
          </tr>
        </tbody>
      </table>
      <div class="row text-right">
        <div class="col-xs-2 col-xs-offset-8">
          <p>
            <strong>
            Данъчна основа : <br>
            Ставка 0% ДДС : <br>
            Общо : <br>
            </strong>
          </p>
        </div>
        <div class="col-xs-2">
          <strong>
          <?php echo $invoice->Amount . \Yii::$app->params['currency']?> <br>
          0 <?= \Yii::$app->params['currency'] ?><br>
          <?php echo $invoice->Amount . \Yii::$app->params['currency']?> <br>
          </strong>
        </div>
      </div>
	  Основание на сделката: чл.113, ал.9 от ЗДДС
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h4>Плащане по сметка</h4>
            </div>
            <div class="panel-body">
              <p>Получател: <?php echo \Yii::$app->params['CompanyName'] ?></p>
              <p>Банка: <?php echo \Yii::$app->params['BankName'] ?></p>
              <p>IBAN : <?php echo \Yii::$app->params['IBAN'] ?></p>
			  <p>Банков код: FINVBGS</p>
            </div>
          </div>
        </div>
        <div class="col-xs-7">
          <div class="span7">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4>Съставил: Атанас Крачев</h4>
              </div>
              <div class="panel-body">
                <p>Подпис на съставителя: АСК11
                </p>
                <h4></h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<small>Съгласно чл.7, ал 1 и чл. 8 от Закона за счетоводството, чл.114 от ЗДДС и чл.78 от ППЗДДС печатът не е задължителен реквизит на
фактурата, а подписите са заменени с идентификационни шифри<small>
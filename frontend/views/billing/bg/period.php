<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\editable\Editable;

$this->title = 'Период на Абонамента';
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Subscription'),
            'url' => ['billing/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
 
?>


<div class="settings">

	<h1><?= Html::encode($this->title) ?></h1>	
    <?php $form = ActiveForm::begin(['action' => ['billing/method']]); ?>			
	<input type="hidden" name="slug" value="<?php echo $slug ?>" />
	<ul>
		<li>
			<input type="radio" name="period" value="1" /> 1 месец = <?= $plan->Price . \Yii::$app->params['currency'] ?>
			<?php 
				if ($customer->Balance < $plan->Price) {
					if ($customer->Balance > 0) {
						echo '(-';
						echo $customer->Balance . \Yii::$app->params['currency'];
						echo ' ще бъдат приспаднати от клиентска Ви сметка)';
					}
				} else {
					echo '(-';
					echo $plan->Price . \Yii::$app->params['currency'];
					echo ' ще бъдат приспаднати от клиентска Ви сметка)';
				}
			?> 
		</li>
		<li>
			<input type="radio" name="period" value="3" /> 3 месецa = <strike><?php echo $plan->Price*3 ?> лв.</strike> <?php echo $plan->Price3m ?> лв. (спестявате <?php echo ($plan->Price*3 - $plan->Price3m) ?> лв.)
			<?php 
				if ($customer->Balance < $plan->Price3m) {
					if ($customer->Balance > 0) {
						echo '(-';
						echo $customer->Balance . \Yii::$app->params['currency'];
						echo ' от клиентската Ви сметка)';
					}
				} else {
					echo '(-';
					echo $plan->Price3m . \Yii::$app->params['currency'];
					echo ' от клиентската Ви сметка)';
				}
			?> 
		</li>
		<li>
			<input type="radio" name="period" value="6" /> 6 месецa = <strike><?php echo $plan->Price*6 ?> лв.</strike> <?php echo $plan->Price6m ?> лв. (спестявате <?php echo ($plan->Price*6 - $plan->Price6m) ?> лв.)
			<?php 
				if ($customer->Balance < $plan->Price6m) {
					if ($customer->Balance > 0) {
						echo '(-';
						echo $customer->Balance . \Yii::$app->params['currency'];
						echo ' от клиентската Ви сметка)';
					}
				} else {
					echo '(-';
					echo $plan->Price6m . \Yii::$app->params['currency'];
					echo ' от клиентската Ви сметка)';
				}
			?> 			
		</li>
		<li>
			<input type="radio" name="period" value="12" checked="checked" /> 12 месецa = <strike><?php echo $plan->Price*12 ?> лв.</strike> <?php echo $plan->Price12m ?> лв. (спестявате <?php echo ($plan->Price*12 - $plan->Price12m) ?> лв.)
			<?php 
				if ($customer->Balance < $plan->Price12m) {
					if ($customer->Balance > 0) {
						echo '(-';
						echo $customer->Balance . \Yii::$app->params['currency'];
						echo ' от клиентската Ви сметка)';
					}
				} else {
					echo '(-';
					echo $plan->Price12m . \Yii::$app->params['currency'];
					echo ' от клиентската Ви сметка)';
				}
			?> 			
		</li>
	</ul>
    <?= Html::submitButton(Yii::t('app', 'Continue'), ['class'=> 'btn btn-primary']) ?>
	
    <?php ActiveForm::end(); ?>



	<h1>Данни за фактура</h1>
	
	<?= Yii::t('app', 'EIK') ?>
	<?php
		echo Editable::widget([
			'model'=>$customer, 
			'attribute' => 'EIK',
			'size' => 'md',
			'format' => 'link',
			'type' => 'post',
			'editableValueOptions'=>['class'=>'well well-sm'],
			'formOptions' => ['method'=>'post', 'action'=> ['billing/editcompany']]
		]);
	?>	
	<br/>
	<?= Yii::t('app', 'Company Name') ?>
	<?php
		echo Editable::widget([
			'model'=>$customer, 
			'attribute' => 'CompanyName',
			'size' => 'md',
			'format' => 'link',
			'type' => 'post',
			'editableValueOptions'=>['class'=>'well well-sm'],
			'formOptions' => ['method'=>'post', 'action'=> ['billing/editcompany']]
		]);
	?>	
	<br/>
	<?= Yii::t('app', 'MOL') ?>
	<?php
		echo Editable::widget([
			'model'=>$customer, 
			'attribute' => 'MOL',
			'size' => 'md',
			'format' => 'link',
			'type' => 'post',
			'editableValueOptions'=>['class'=>'well well-sm'],
			'formOptions' => ['method'=>'post', 'action'=> ['billing/editcompany']]
		]);
	?>	
	<br/>
	<?= Yii::t('app', 'IN_DDS') ?> (<?= Yii::t('app', 'Optional') ?>)
	<?php
		echo Editable::widget([
			'model'=>$customer, 
			'attribute' => 'IN_DDS',
			'size' => 'md',
			'format' => 'link',
			'type' => 'post',
			'editableValueOptions'=>['class'=>'well well-sm'],
			'formOptions' => ['method'=>'post', 'action'=> ['billing/editcompany']]
		]);
	?>	
	<br/>
	<?= Yii::t('app', 'City') ?>
	<?php
		echo Editable::widget([
			'model'=>$customer, 
			'attribute' => 'City',
			'size' => 'md',
			'format' => 'link',
			'type' => 'post',
			'editableValueOptions'=>['class'=>'well well-sm'],
			'formOptions' => ['method'=>'post', 'action'=> ['billing/editcompany']]
		]);
	?>	
	<?= Yii::t('app', 'Address') ?>
	<?php
		echo Editable::widget([
			'model'=>$customer, 
			'attribute' => 'Address',
			'size' => 'md',
			'format' => 'link',
			'type' => 'post',
			'editableValueOptions'=>['class'=>'well well-sm'],
			'formOptions' => ['method'=>'post', 'action'=> ['billing/editcompany']]
		]);
	?>	
	
</div><!-- settings -->

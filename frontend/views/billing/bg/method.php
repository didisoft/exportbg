<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\editable\Editable;

$this->title = 'Метод на плащане';
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Subscription'),
            'url' => ['billing/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
 
?>
<style>
.c3col {
  float: left;
  width: 210px;
  height: 180px;
  background: url("<?php echo Yii::$app->homeUrl ?>/images/payBackground.png") no-repeat scroll 0 0 transparent;
  text-align: center;
  margin: 0px 10px 30px 10px;
}
.col_img {
  height: 100px;
  margin-top: 20px; 
}
.col_foot {
height: 60px;
}
</style>

<h1>Метод на плащане</h1>
<div class="settings">

	<div class="c3col">
		<div class="col_img">
			<img src="<?php echo Yii::$app->homeUrl ?>/images/pay-BANK.png" border="0" id="Bank">
		</div>
		<div class="col_foot">
			<a href="<?= Yii::$app->urlManager->createUrl(['billing/bank-statement', 'slug' => $slug, 'period' => $period]) ?>" class="btn btn-info">Банков Превод</a>
		</div>
	</div>
	<div class="c3col">
		<div class="col_img">
			<img src="<?php echo Yii::$app->homeUrl ?>/images/pay-EPAY.png" border="0" id="EPAY">
		</div>
		<div class="col_foot">
			<a href="<?= Yii::$app->urlManager->createUrl(['billing/epay-statement', 'type' => 'login', 'slug' => $slug, 'period' => $period]) ?>" class="btn btn-info">Чрез ePay.bg</a>
		</div>
	</div>
	<div class="c3col">
		<div class="col_img">
			<img src="<?php echo Yii::$app->homeUrl ?>/images/visa-mastercard.gif" border="0" id="EPAY">
		</div>
		<div class="col_foot">
			<a href="<?= Yii::$app->urlManager->createUrl(['billing/epay-statement', 'type' => 'card', 'slug' => $slug, 'period' => $period]) ?>" class="btn btn-info">Кредитна Карта</a>
		</div>
	</div>
	
</div><!-- settings -->

<style>
div {
display: block;
overflow: hidden;
width: auto;
}
.platejno {
width: 600px;
margin-bottom: 15px;

}
.platejno div {
width: auto;
zoom: 1;
position: relative;
text-align: left;
}
.platejno_row {
border-left: 2px solid black;
border-right: 2px solid black;
border-top: 2px solid black;
padding: 3px 4px;
}
.platejno_row_title {
font-size: 11px;
padding-bottom: 2px;
}
.platejno_highlight {
background: #C8E8FF;
padding: 0px 4px 2px;
}
.platejno_right {
float: right;
}
.platejno_left {
float: left;
}
.platejno_inp {
border: 1px solid black;
background: url('<?php echo Yii::$app->homeUrl; ?>/images/platejno-inp.png') #ffffff 16px 100% repeat-x;
font-family: 'Bitstream Vera Sans Mono', 'Courier New', Courier, monospace;
font-size: 12px;
letter-spacing: 9px;
padding: 10px 0 0 3px;
vertical-align: middle;
width: 585px;
height: 12px;
font-weight: bold;
text-transform: uppercase;
line-height: 10px;
display: inline-block;
}

@media print {
    a {
        display: none;
    }
}
</style>

<a href="javascript:print()">ОТПЕЧАТАЙ</a>

<div class="platejno printable">
    <div class="platejno_row">
        <div class="platejno_row_title">Платете на - име на получателя</div>
        <input type="text" class="platejno_inp" size="35" value="ЕРЕПОРТ БЪЛГАРИЯ ЕООД">
    </div>
    <div class="platejno_row" style="padding-left:0; padding-right:0;">
        <div class="platejno_left platejno_highlight" style="_width:365px;">
            <div class="platejno_row_title">IBAN на получателя</div>
            <input type="text" style="width:360px;" class="platejno_inp" size="35" readonly="readonly" value="BG74FINV91501016421151">
        </div>
        <div class="platejno_right platejno_highlight" style="_width:140px;">
            <div class="platejno_row_title">BIC на получателя</div>
            <input type="text" style="width:135px;" class="platejno_inp" size="35" readonly="readonly" value="FINVBGSF">
        </div>
    </div>
    <div class="platejno_row">
        <div class="platejno_row_title">При банка</div>
        <input type="text" class="platejno_inp" size="35" readonly="readonly" value="Първа инвестиционна банка">
    </div>
    <div class="platejno_row" style="padding-right:0;">
        <div class="platejno_left" style="_width:222px;">
            <div class="platejno_big_title">ПРЕВОДНО НАРЕЖДАНЕ</div>
            <div class="platejno_small_title">за кредитен превод</div>
        </div>
        <div class="platejno_right platejno_highlight" style="_width:279px;">
            <div class="platejno_row_title">Сума</div>
            <input type="text" class="platejno_inp" readonly="readonly" style="text-align:right; width: 274px; *width:268px; *padding-right:3px;" size="35" value="<?php echo $order->Amount ?>">
        </div>
        <div class="platejno_right platejno_highlight" style="_width:50px; margin-right: 10px; *margin-right:5px;">
            <div class="platejno_row_title">Валута</div>
            <input type="text" class="platejno_inp" readonly="readonly" style="width:44px;" size="35" value="BGN">
        </div>
    </div>
    <div class="platejno_row">
        <div class="platejno_row_title">Основание за плащане</div>
        <input type="text" class="platejno_inp" readonly="readonly" size="35" value="Плащане на абонамент <?php echo $order->OrderID ?>">
    </div>
    <div class="platejno_row">
        <div class="platejno_row_title">Още пояснения</div>
        <input type="text" class="platejno_inp" readonly="readonly" size="35" value="www.eReport.bg">
    </div>
    <div class="platejno_row">
        <div class="platejno_row_title">Наредител - име на наредителя</div>
        <input type="text" class="platejno_inp" readonly="readonly" size="35" value="<?php echo $order->CompanyName ?>">
    </div>
    <div class="platejno_row" style="padding-left:0; padding-right:0;">
        <div class="platejno_left platejno_highlight" style="_width:365px;">
            <div class="platejno_row_title">IBAN на наредителя</div>
            <input type="text" class="platejno_inp" readonly="readonly" style="width:360px;" size="35">
        </div>
        <div class="platejno_right platejno_highlight" style="_width:145px;">
            <div class="platejno_row_title">BIC на наредителя</div>
            <input type="text" class="platejno_inp" readonly="readonly" size="35" style="width:140px;">
        </div>
        <div class="platejno_right platejno_highlight" style="margin-right:6px; *margin-right:4px;_width:50px;">
            <div class="platejno_row_title">Валута</div>
            <input type="text" class="platejno_inp" readonly="readonly" size="35" style="width:45px;">
        </div>
    </div>
    <div class="platejno_row" style=" border-bottom:2px solid black;">
        <div class="platejno_left" style="_width:280px;">
            <div class="platejno_row_title">Платежна система:</div>
            <input type="text" class="platejno_inp" readonly="readonly" style="text-align:right; width: 274px;" size="15">
        </div>
        <div class="platejno_right" style="_width:175px;">
            <div class="platejno_row_title">Вальор:</div>
            <input type="text" class="platejno_inp" readonly="readonly" style="width:170px;" size="35" value="">
        </div>
    </div>
</div>

<a href="javascript:print()">ОТПЕЧАТАЙ</a>	
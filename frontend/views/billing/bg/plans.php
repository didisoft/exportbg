<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */

$this->title = 'Избор на Абонаментен план';
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Subscription'),
            'url' => ['billing/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->homeUrl ?>plans-style.css" />
<div class="settings">

    <?php $form = ActiveForm::begin(); ?>
	
  <div class="plans">
	<?php if ($plan4 != null) { ?>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan4->Name ?></h3>
      <p class="plan-price"><?php echo $plan4->Price ?><span class="plan-unit">лв. на месец</span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan4->WordsCount ?> <span class="plan-feature-name">фрази</span></li>
        <li class="plan-feature"><?php echo $plan4->LoginsCount ?><span class="plan-feature-unit"></span> <span class="plan-feature-name">потребители</span></li>
      </ul>
      <?php 
		if ($customer->PlanID == $plan4->PlanID) {
			echo Html::a(Yii::t('app', 'Extend'), ['billing/extend'], ['class'=>'btn btn-success']);
		} else if ($customer->PlanID < $plan4->PlanID) {
			echo Html::a(Yii::t('app', 'Select'), ['billing/period', 'id' => $plan4->PlanID], ['class'=>'btn btn-info']); 
		} else {
			echo '-';
		}
	  ?>
    </div>
	<?php } ?>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan3->Name ?></h3>
      <p class="plan-price"><?php echo $plan3->Price ?><span class="plan-unit">лв. на месец</span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan3->WordsCount ?> <span class="plan-feature-name">фрази</span></li>
        <li class="plan-feature"><?php echo $plan3->LoginsCount ?><span class="plan-feature-unit"></span> <span class="plan-feature-name">потребители</span></li>
      </ul>
      <?php 
		if ($customer->PlanID == $plan3->PlanID) {
			echo Html::a(Yii::t('app', 'Extend'), ['billing/extend'], ['class'=>'btn btn-success']);
		} else if ($customer->PlanID < $plan3->PlanID) {
			echo Html::a(Yii::t('app', 'Select'), ['billing/period', 'id' => $plan3->PlanID], ['class'=>'btn btn-info']); 
		} else {
			echo '-';
		}
	  ?>
    </div>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan2->Name ?></h3>
      <p class="plan-price"><?php echo $plan2->Price ?><span class="plan-unit">лв. на месец</span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan2->WordsCount ?> <span class="plan-feature-name">фрази</span></li>
        <li class="plan-feature"><?php echo $plan2->LoginsCount ?><span class="plan-feature-unit"></span> <span class="plan-feature-name">потребители</span></li>
      </ul>
      <?php 
		if ($customer->PlanID == $plan2->PlanID) {
			echo Html::a(Yii::t('app', 'Extend'), ['billing/extend'], ['class'=>'btn btn-success']);
		} else if ($customer->PlanID < $plan2->PlanID) {
			echo Html::a(Yii::t('app', 'Select'), ['billing/period', 'id' => $plan2->PlanID], ['class'=>'btn btn-info']); 
		} else {
			echo '-';
		}
	  ?>
    </div>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan1->Name ?></h3>
      <p class="plan-price"><?php echo $plan1->Price ?><span class="plan-unit">лв. на месец</span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan1->WordsCount ?> <span class="plan-feature-name">фрази</span></li>
        <li class="plan-feature"><?php echo $plan1->LoginsCount ?><span class="plan-feature-unit"></span> <span class="plan-feature-name">потребител</span></li>
      </ul>
      <?php 
		if ($customer->PlanID == $plan1->PlanID) {
			echo Html::a(Yii::t('app', 'Extend'), ['billing/extend'], ['class'=>'btn btn-success']);
		} else if ($customer->PlanID < $plan1->PlanID) {
			echo Html::a(Yii::t('app', 'Select'), ['billing/period', 'id' => $plan1->PlanID], ['class'=>'btn btn-info']); 
		} else {
			echo '-';
		}
		?>
    </div>
  </div>
	

    <?php ActiveForm::end(); ?>

</div><!-- settings -->

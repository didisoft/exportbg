<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */
?>
<div class="settings">

    <?php $form = ActiveForm::begin(['action' => ['billing/statement']]); ?>
	
	Банков превод

	<?php echo $form->field($model, 'CompanyName')->textInput() ?>
	<?php echo $form->field($model, 'EIK')->textInput() ?>
	<?php echo $form->field($model, 'MOL')->textInput() ?>
	<?php echo $form->field($model, 'Address')->textInput() ?>
    <?= Html::submitButton('Submit') ?>
	
    <?php ActiveForm::end(); ?>

</div><!-- settings -->

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

use frontend\widgets\HintWidget;
use kartik\popover\PopoverX;

$this->title = Yii::t('app', 'Subscription');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>
<div class="settings">

	<table class="table">
	<tr>
		<td>
		<?php echo Yii::t('app', 'Subscription plan') ?>
		</td>
		<td>
		<b><?= $plan->Name ?></b>
		</td>
		<td>
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => Yii::t('app', 'Your current subscription plan'),
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>	
		</td>
		<td>
		<?php echo Html::a(Yii::t('app', 'Extend subscription'), ['billing/extend'], ['class' => 'btn btn-info']) ?>	
		<?php if ($customer->PlanID > 0 && $customer->PlanID < MAX_PLAN_ID) { ?>
		<?php 
			echo Html::a(Yii::t('app', 'Change plan'), ['billing/plan'], ['class' => 'btn btn-success']);
		}
		?>	
		</td>
	</tr>
	<tr>
		<td>
		<?php echo Yii::t('app', 'Next Billing date') ?>
		</td>
		<td>
		<?php $f = new \yii\i18n\Formatter(); echo $f->asDate($customer->PlanExpirationTime); ?>
		<?php if (strtotime($customer->PlanExpirationTime) < time()) { ?>
			<span style="color:red"><?php echo Yii::t('app', 'Expired') ?></span>
		<?php } ?>
		</td>
		<td>
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => Yii::t('app', 'The new plan becomes active after successful order'),
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>
		</td>
	</tr>
	<tr>
		<td>
		<?php echo Yii::t('app', 'Words tracked') ?>
		</td>
		<td>
		<b><?= $words ?>/<?= $plan->WordsCount ?></b>
		(<?php echo \yii\helpers\Html::a(Yii::t('app', 'Details'), ['words/index']) ?>)
		</td>
		<td>
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => Yii::t('app', 'How much of the available words/phrases for tracked you are using'),
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>	
		</td>	
	</tr>
	<?php if (\Yii::$app->language === 'bg-BG') { ?>
	<tr>
		<td>
		Клиентска сметка: 
		</td>
		<td>
		<b><?php echo $customer->Balance ?></b> лв.	
		</td>
		<td>
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => 'Натрупана сума по клиентската Ви сметка. Наличната по клиентската сметка сума може да бъде ползвана за заплащане на всички предлагани от eReport.bg услуги',
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>	
		</td>		
	</tr>
	<?php } ?>
	</table>


</div><!-- settings -->

<h2><?php echo Yii::t('app', 'Issued Invoices') ?></h2>
<?php
echo Html::a(Yii::t('app', 'Issued Invoices Archive'), ['billing/invoices'], ['class' => 'btn btn-info']);
echo '&nbsp;';
echo Html::a(Yii::t('app', 'Company details'), ['settings/company'], ['class' => 'btn btn-info']);
?>

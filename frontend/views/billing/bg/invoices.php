<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

use frontend\widgets\HintWidget;
use kartik\popover\PopoverX;

$this->title = Yii::t('app', 'Issued Invoices');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Subscription'),
            'url' => ['billing/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?php echo Yii::t('app', 'Issued Invoices') ?></h1>
<?php
echo GridView::widget([
    'dataProvider' => $invoiceProvider,
    'columns' => [
        'InvoiceDate:date',
        'OrderText',
		'Amount',
		[        
			'class' => \yii\grid\ActionColumn::className(),        
			'template' => '{view}',
			'urlCreator' => function ($action, $model, $key, $index) {
				if ($action === 'view') {
					$url = 'invoice?id=' .$model->getPrimaryKey();
					return $url;
				}
			},
			'buttons' => [
				'view' => function ($url, $model, $key) {
					return Html::a(Yii::t('app', 'Details'), $url);
				}
			],	
			// you may configure additional properties here  
		],		
	],	
]);	
?>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

use frontend\widgets\HintWidget;
use kartik\popover\PopoverX;

$this->title = Yii::t('app', 'Subscription');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
            <!-- form start -->
    <div class="box-body">

	<table class="table">
	<tr>
		<td>
		<?php echo Yii::t('app', 'Subscription plan') ?>
		</td>
		<td>
		<b><?= $plan->Name ?></b>
		</td>
		<td>
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => Yii::t('app', 'Your current subscription plan'),
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>	
		</td>
		<td>
		<?php 
			echo Html::a(Yii::t('app', 'Change plan'), \Yii::$app->getUrlManager()->createUrl(['clickbank/plan']), ['class' => 'btn btn-success']);
		?>	
        <?php 
            if ($customer->PlanID != 0) {
                echo Html::a(Yii::t('app', 'Manage subscription'), $customer->FastSpringUrl,  ['target' => '_blank']);
            }
		?>			
        </td>
	</tr>
	<tr>
		<td>
		<?php echo Yii::t('app', 'Words tracked') ?>
		</td>
		<td>
		<b><?= $words ?>/<?= $plan->WordsCount ?></b>
		(<?php echo \yii\helpers\Html::a(Yii::t('app', 'Details'), ['words/index']) ?>)
		</td>
		<td>
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => Yii::t('app', 'How much of the available words/phrases for tracked you are using'),
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
			]);	
		?>	
		</td>	
	</tr>
	</table>

    </div>
</div><!-- settings -->


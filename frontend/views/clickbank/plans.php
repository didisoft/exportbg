<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Subscription plan');
$this->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Subscription'),
            'url' => ['billing/index'],
        ];
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->homeUrl ?>plans-style.css" />
<!-- ClickBank Trust Badge -->
<script src='//cbtb.clickbank.net/?vendor=listen6'></script>
<div class="box box-primary">
            <!-- form start -->
    <div class="box-body">

    <?php $form = ActiveForm::begin(); ?>
	
  <div class="plans">
	<?php if ($plan4 != null) { ?>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan4->Name ?></h3>
      <p class="plan-price"><?php echo $plan4->Price ?><span class="plan-unit"><?php echo Yii::t('app', '$/month') ?></span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan4->WordsCount ?> <span class="plan-feature-name"><?php echo Yii::t('app', 'words tracked') ?></span></li>
        <li class="plan-feature"><?php echo $plan4->LoginsCount ?><span class="plan-feature-unit"></span> <span class="plan-feature-name"><?php echo Yii::t('app', 'user accounts') ?></span></li>
      </ul>
      <?php 
		$tmpPlan = $plan4;
		if ($customer->PlanActive==1) {
			if ($customer->PlanID != $tmpPlan->PlanID) {
				echo Html::a(Yii::t('app', 'Change plan'), ['upgrade', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 
			}
		} else {
			echo Html::a(Yii::t('app', 'Select'), ['order', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info', 'target' => '_blank']); 			
		}
	  ?>
    </div>
	<?php } ?>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan3->Name ?></h3>
      <p class="plan-price"><?php echo $plan3->Price ?><span class="plan-unit"><?php echo Yii::t('app', '$/month') ?></span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan3->WordsCount ?> <span class="plan-feature-name"><?php echo Yii::t('app', 'words tracked') ?></span></li>
        <li class="plan-feature"><?php echo $plan3->LoginsCount ?><span class="plan-feature-unit"></span> <span class="plan-feature-name"><?php echo Yii::t('app', 'user accounts') ?></span></li>
      </ul>
      <?php 
		$tmpPlan = $plan3;
		if ($customer->PlanActive==1) {
			if ($customer->PlanID != $tmpPlan->PlanID) {
				echo Html::a(Yii::t('app', 'Change plan'), ['upgrade', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 
			}
		} else {
			echo Html::a(Yii::t('app', 'Select'), ['order', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info', 'target' => '_blank']); 			
		}
	  ?>
    </div>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan2->Name ?></h3>
      <p class="plan-price"><?php echo $plan2->Price ?><span class="plan-unit"><?php echo Yii::t('app', '$/month') ?></span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan2->WordsCount ?> <span class="plan-feature-name"><?php echo Yii::t('app', 'words tracked') ?></span></li>
        <li class="plan-feature"><?php echo $plan2->LoginsCount ?><span class="plan-feature-unit"></span> <span class="plan-feature-name"><?php echo Yii::t('app', 'user accounts') ?></span></li>
      </ul>
      <?php 
		$tmpPlan = $plan2;
		if ($customer->PlanActive==1) {
			if ($customer->PlanID != $tmpPlan->PlanID) {
				echo Html::a(Yii::t('app', 'Change plan'), ['upgrade', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 
			}
		} else {
			echo Html::a(Yii::t('app', 'Select'), ['order', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info', 'target' => '_blank']); 			
		}
	  ?>
    </div>
    <div class="plan">
      <h3 class="plan-title"><?php echo $plan1->Name ?></h3>
      <p class="plan-price"><?php echo $plan1->Price ?><span class="plan-unit"><?php echo Yii::t('app', '$/month') ?></span></p>
      <ul class="plan-features">
        <li class="plan-feature"><?php echo $plan1->WordsCount ?> <span class="plan-feature-name"><?php echo Yii::t('app', 'words tracked') ?></span></li>
        <li class="plan-feature"><?php echo $plan1->LoginsCount ?><span class="plan-feature-unit"></span> <span class="plan-feature-name"><?php echo Yii::t('app', 'user accounts') ?></span></li>
      </ul>
      <?php 
		$tmpPlan = $plan1;
		if ($customer->PlanActive==1) {
			if ($customer->PlanID != $tmpPlan->PlanID) {
				echo Html::a(Yii::t('app', 'Change plan'), ['upgrade', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info']); 
			}
		} else {
			echo Html::a(Yii::t('app', 'Select'), ['order', 'id' => $tmpPlan->PlanID], ['class'=>'btn btn-info', 'target' => '_blank']); 			
		}
		?>
    </div>
  </div>
	

    <?php ActiveForm::end(); ?>

    </div>
</div><!-- settings -->

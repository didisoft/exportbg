<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Logins */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Clickbank payment information');
$this->params['breadcrumbs'][] = $this->title;

function getRequest($name) {
    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : '';
}

?>
<p>
Thank you for your order.
</p>

<p>
<?php
	$messages= "<tr style='background: #f6f6f6;'><td><h3>ClickBank Receipt:</h3> </td>
		<td></td></tr>";
	$messages.= "<tr><td><strong>Transacton ID #:</strong> </td>
		<td>" . strip_tags(getRequest('cbreceipt')) . "</td></tr>";
	$messages.= "<tr><td><strong>Item #:</strong> </td>
		<td>" . strip_tags(getRequest('item')) . "</td></tr>";
	$messages.= "<tr><td><strong>Customer Name:</strong> </td>
		<td>" . strip_tags(getRequest('cname')) . "</td></tr>";				
	$messages.= "<tr><td><strong>Customer Email:</strong> </td>
		<td>" . strip_tags(getRequest('cemail')) . "</td></tr>";		
    
    echo '<table>';
    echo $messages;
    echo '</table>';    
?>
    
</p>

<p>
    You shall receive within five minutes an email at <?= getRequest('cemail') ?> 
    with login details to our Customers' section where you can setup the words that you need tracked by <?= \Yii::$app->params['siteName'] ?>.
</p>


<p>
    Should you have any questions regarding your order, please don't hesitate to <a href="/support/">drop us a line</a>
</p>

<p>
    Please note that your credit card statement will show a charge from <b>CLKBANK*</b>
</p>

<p>
    <i>
ClickBank is the retailer of this product. CLICKBANK® is a registered
trademark of Click Sales, Inc., a Delaware corporation located at 1444 S. Entertainment
Ave., Suite 410 Boise, ID 83709, USA and used by permission. ClickBank's role as retailer
does not constitute an endorsement, approval or review of this product or any claim,
statement or opinion used in promotion of this product.        
    </i>
</p>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Words');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
            <!-- form start -->
    <div class="box-body">

    <p>	
	<?php 
	if ($plan->WordsCount > $dataProvider->count) { ?>	
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>		
		<?php
			echo PopoverX::widget([
				'header' => Yii::t('app', 'Information'),
				'type' => PopoverX::TYPE_INFO,
				'placement' => PopoverX::ALIGN_RIGHT,
				'content' => Yii::t('app', 'Enter here the words/phrases that you wish the system to monitor on the Internet'),
				'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info'],
			]);	
		?>					
	<?php } else { 
		echo Yii::t('app', 'You\'ve reached the maximum of {wordsCount} words for your subscription plan',
			['wordsCount' => $plan->WordsCount]);
	} ?>	
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'WordText',
            'EnteredOn',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'], // words can only be deleted, not edited
        ],
    ]); ?>

    </div>
</div>

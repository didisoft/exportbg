<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $model app\models\Words */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Words');
?>

<div class="box box-primary">
            <!-- form start -->
    <div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

	<?php if ($model->isNewRecord) { ?>
	<?php
		echo PopoverX::widget([
			'header' => Yii::t('app', 'Information'),
			'type' => PopoverX::TYPE_INFO,
			'placement' => PopoverX::ALIGN_RIGHT,
			'content' => Yii::t('app', 'Enter here the words/phrases that you wish the system to monitor on the Internet'),
			'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
		]);	
	?>	
    <?= $form->field($model, 'WordText')->textInput(['style'=>'width:300px', 'maxlength' => 255]) ?>
	<?php } else { ?>
	<?php
		echo PopoverX::widget([
			'header' => Yii::t('app', 'Information'),
			'type' => PopoverX::TYPE_INFO,
			'placement' => PopoverX::ALIGN_RIGHT,
			'content' => Yii::t('app', 'The text of already entered words cannot be edited. If no longeed need a word/phrase, just delete it and then add a new one'),
			'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
		]);	
	?>	
	<div class="form-group">
	<label>	
		<?php echo Yii::t('app', 'Word Text') ?>:
	<label>	
	<?php echo $model->WordText; ?>
	</div>	
	<?php } ?>

	<?php
		echo PopoverX::widget([
			'header' => Yii::t('app', 'Information'),
			'type' => PopoverX::TYPE_INFO,
			'placement' => PopoverX::ALIGN_RIGHT,
			'content' => Yii::t('app', 'Select only if you want the monitored term to be exactly matched'),
			'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
		]);	
	?>	
	<?= $form->field($model, 'ExactMatch')->checkbox() ?>

	
<div class="form-group">	
(+) <a href='#' class='advanced'><?php echo Yii::t('app', 'Advanced Options') ?></a>
</div>

<div id="advancedOptions">
	<?php
		echo PopoverX::widget([
			'header' => Yii::t('app', 'Information'),
			'type' => PopoverX::TYPE_INFO,
			'placement' => PopoverX::ALIGN_RIGHT,
			'content' => Yii::t('app', 'Specify here extra terms that you want to be found in the results'),
			'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
		]);	
	?>	
	<?= $form->field($model, 'OrTerms')->textInput(['maxlength' => 2000]) ?>
	<?php
		echo PopoverX::widget([
			'header' => Yii::t('app', 'Information'),
			'type' => PopoverX::TYPE_INFO,
			'placement' => PopoverX::ALIGN_RIGHT,
			'content' => Yii::t('app', 'Specify here words or phrases that shouldn\'t be found in the results'),
			'toggleButton' => ['label'=>'?', 'class'=>'btn btn-info btn-xs'],
		]);	
	?>	
	<?= $form->field($model, 'ExcludeTerms')->textInput(['maxlength' => 200]) ?>
</div>
<?php $jsAdvanced = <<<EOT
    $(document).ready(function () {
        $('#advancedOptions').hide();
        $('.advanced').click(function() {
            if ($('#advancedOptions').is(':hidden')) {
                 $('#advancedOptions').slideDown();
            } else {
                 $('#advancedOptions').slideUp();
            }
        });
    });
EOT;
$this->registerJs($jsAdvanced); 	
?>	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Insert') : Yii::t('app', 'Submit'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Cancel'), ['words/index']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\web\JsExpression;
use yii\helpers\Url;
use smallbearsoft\ajax\Ajax;

use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $model app\models\Words */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Words');
?>

<div class="modal-dialog" role="document">
	<div class="modal-content">

    <?php $form = ActiveForm::begin(); ?>
	  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= $this->title ?></h4>
      </div>
	  <div class="modal-body">
	  <?= $form->errorSummary($model); ?>        

	<div class="form-group">
	<label>	
		<?php echo Yii::t('app', 'Word Text') ?>:
	<label>	
	<?php echo $model->WordText; ?>
	</div>	

    <div>
        <?php echo $mention->Snippet; ?>                  
    </div>      
          
	<?= $form->field($model, 'ExcludeTerms')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Insert') : Yii::t('app', 'Submit'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Cancel') ?></button>
    </div>

	  </div>
      <div class="modal-footer">
<?php
    Ajax::begin(['clientOptions' => [
    'success' => new JsExpression('function(data, textStatus, jqXHR) {alert(data)}'),
    'error' => new JsExpression('function(jqXHR, textStatus, errorThrown) {alert(errorThrown)}'),
    'beforeSend' => new JsExpression('function(jqXHR, settings) {alert("Before send.")}'),
    'complete' => new JsExpression('function(jqXHR, textStatus) {alert("Complete.")}'),
    'timeout' => 10000
]]) ?>
    <a href="<?= Url::to(['mentions/spam', 'id'=>$mention->MentionID]) ?>" data-ajax="1">Mark as Spam</a>
<?php Ajax::end() ?>                
          
       <?= Html::button(Yii::t('app', 'OK'), ['onclick' => "submitNewForm(); return true;", 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Cancel') ?></button>
      </div>        
    <?php 
        ActiveForm::end(); 
    ?>
    </div>
</div>

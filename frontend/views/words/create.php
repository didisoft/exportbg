<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Words */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Words'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'New record');
?>
<div class="words-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

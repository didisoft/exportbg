<table cellspacing="0" class="has-feed">
	<tbody>
		<?php $i = 0;
		foreach ($results as $result) { 
			$i++;
		?>
		<tr class="alt">
			<td>
				<a href="<?php echo $result['Link'] ?>" mc:disable-tracking>
					<?php echo $result['Title'] ?>		
				</a>	
				<br/>
				<span>
					<?php echo str_ireplace($word, '<b>'.$word.'</b>', $result['Snippet']); ?>
				</span>
				<br/>
				<span>
                    <b><?php echo Yii::$app->formatter->asDate($result['MentionDate']) ?></b>
				</span>
				<br/>				
				<span>
					<a href="<?php echo $result['Link'] ?>" rel="nofollow" mc:disable-tracking>
					 <?php 
						$url = $result['Link'];
						$parts = parse_url($url);
						echo $parts['scheme'] . '://' . $parts['host'];										
					?>
					</a>
				</span>								
				<br/>
				&nbsp;
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
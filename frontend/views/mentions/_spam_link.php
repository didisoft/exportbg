<?php
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;
use smallbearsoft\ajax\Ajax;

Ajax::begin(['clientOptions' => [
    'success' => new JsExpression('function(data, textStatus, jqXHR) {$("spam_' . $model->MentionID .'").hide();}'),
    'error' => new JsExpression('function(jqXHR, textStatus, errorThrown) {}'),
    'beforeSend' => new JsExpression('function(jqXHR, settings) {}'),
    'complete' => new JsExpression('function(jqXHR, textStatus) {}'),
    'timeout' => 10000
]]) ?>
    <a id="spam_<?php echo $model->MentionID ?>" href="<?= Url::to(['mentions/spam', 'id'=>$model->MentionID]) ?>" class="btn btn-danger" data-ajax="0"><?= Yii::t('app', 'Mark as Spam') ?></a>
<?php Ajax::end() ?>                

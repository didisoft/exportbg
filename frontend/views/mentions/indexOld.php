<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;


/* @var $this yii\web\View */
/* @var $searchModel app\models\MentionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mentions');
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="modalEdit" class="modal remote fade" role="dialog"></div>    

<div class="box box-primary">
            <!-- form start -->
              <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php $form = ActiveForm::begin(); ?>

<div style="float: left;">
<table>
<tr>
	<td>
	<?= Yii::t('app', 'Period') ?>&nbsp;
	</td>
	<td>
	<?php
		echo Html::activeDropDownList( $searchForm, 'period', 
							[TODAY => Yii::t('app', 'Today'),
							YESTERDAY => Yii::t('app', 'Yesterday'),
							DAYS7 => Yii::t('app', 'Last 7 Days'),
							DAYS30 => Yii::t('app', 'Last 30 days'),
							THIS_MONTH => Yii::t('app', 'This month'),
							CUSTOM_PERIOD => Yii::t('app', 'Custom')],
						['class'=>'form-control',
						 'onchange'=>'if (this.value == '.CUSTOM_PERIOD.') {$("#mentionssearchform-from").prop("disabled",false);$("#mentionssearchform-to").prop("disabled",false);$("#mentionssearchform-from").select();} else {$(\'#mentionssearchform-from\').prop("disabled", true);$(\'#mentionssearchform-to\').prop("disabled", true);}'
						]);	
	?>
	</td>
	<td>&nbsp;
	<?= Yii::t('app', 'Word') ?>&nbsp;
	</td>
	<td>
	<?php
		echo Html::activeDropDownList( $searchForm, 'WordID', 
							ArrayHelper::merge(['0'=>'-'], ArrayHelper::map($words, 'WordID', 'WordText')),
						['class'=>'form-control',
						 'onchange'=>'if (this.value == '.CUSTOM_PERIOD.') {$("#mentionssearchform-from").prop("disabled",false);$("#mentionssearchform-to").prop("disabled",false);$("#mentionssearchform-from").select();} else {$(\'#mentionssearchform-from\').prop("disabled", true);$(\'#mentionssearchform-to\').prop("disabled", true);}'
						]);	
	?>
	</td>
	<td>&nbsp;
	<?= Html::submitButton(Yii::t('app', 'Show'), ['class'=>'btn btn-success']) ?>
	</td>
</tr>
<tr>
	<td>
	&nbsp;
	</td>
</tr>
<tr>
	<td align="right"> 
	<?= Yii::t('app', 'From') ?>&nbsp;
	</td>
	<td>
	<?php echo DatePicker::widget([
		'model' => $searchForm, 
		'attribute' => 'from',
		'type' => DatePicker::TYPE_INPUT,
		'pluginOptions' => [
			'autoclose'=>true,
			'format' => 'd/m/yyyy'
		],
		'options' => ($searchForm->period == CUSTOM_PERIOD ? [] : ['disabled'=>'true']),
	]);
	?>
	</td>
	<td align="right">
	&nbsp;
	<?= Yii::t('app', 'To') ?>
	&nbsp;
	</td>
	<td>
	<?php echo DatePicker::widget([
		'model' => $searchForm, 
		'attribute' => 'to',
		'type' => DatePicker::TYPE_INPUT,
		'pluginOptions' => [
			'autoclose'=>true,
			'format' => 'd/m/yyyy'
		],
		'options' => ($searchForm->period == CUSTOM_PERIOD ? [] : ['disabled'=>'true']),		
	]);
	?>
	</td>
</tr>
</table>
</div>
<div style="float: right">
<img src="<?php echo Yii::$app->homeUrl?>\images\star1.png" height=16 width=16 alt="<?php echo Yii::t('app','Favorites'); ?>" />
<?= Html::a(Yii::t('app','Favorites'), ['mentions/show-favorites']) ?>
</div>
<div style="clear: both">
&nbsp;


<?php ActiveForm::end();  

$f = new \yii\i18n\Formatter(); 
?>

<div id="loadingDiv" style="display: none">
<img src="<?php echo Yii::$app->homeUrl?>\images\loading.gif" height="80" width="80" style="z-index:100; position: fixed; top:50%; left:50%; margin:-100px 0 0 -100px; width:80px; height:80px;  " />
</div>
<?php //Pjax::begin(); ?>
<table width="100%">
<?php 
$line = 0;
foreach ($models as $model) {
 if (++$line%2 == 1) {$style = ' style="background-color: #F8F8F8"';} else {$style='';};
?>
<tr<?= $style ?>>
 <td style="padding: 5px;">
	<div style="float: left; width: 90%">
		<b><?php echo Html::a($model->Title, $model->Link, ['target' => '_blank']); ?></b>
		<br/>
		<span id="snippet"><?php echo $model->Snippet; ?></span>
		<br/>
		<?php 
			$url = $model->Link;
			$parts = parse_url($url);
			echo Html::a( $parts['scheme'] . '://' . $parts['host'], $model->Link, ['target' => '_blank']);									
		?>
	</div>
	<div style="float: right; width: 100px">
		<?php echo $f->asDate($model->MentionDate); ?>
		<div id='ajax_result_<?php echo $model->MentionID ?>'>
		<img src="<?php echo Yii::$app->homeUrl?>\images\star<?php echo $model->Favorites ?>.png" height=16 width=16 alt="<?php echo Yii::t('app','Favorites'); ?>" />
		</div>
		<?php //Yii::$app->homeUrl
		$js = "
				   $('#ajax_result_".$model->MentionID."').click(function(){
					 $.ajax({
						url: '".Yii::$app->urlManager->createAbsoluteUrl(['mentions/favorites', 'id'=>$model->MentionID])."',
						success: function(data) {
							$('#ajax_result_".$model->MentionID."').html(data.body)
						}
				})
			});
		";
				
		$this->registerJs( $js); 		
		?>
	</div>
</td>	
</tr>	
<?php 	
}
?>	
</table>
<?php 	
// display pagination
echo LinkPager::widget([
    'pagination' => $pages,
	'registerLinkTags' => true,
	'hideOnSinglePage' => false
]);
?>	
<?php //Pjax::end(); 
$progress = <<<EOT
var ajax_cnt = 0;
var g_loading = $('#loadingDiv').hide();
$(document)
  .ajaxStart(function () {
    g_loading.show();
	ajax_cnt++;
  })
  .ajaxStop(function () {
    ajax_cnt--;
    if (ajax_cnt <= 0) {  
      g_loading.hide();
	  ajax_cnt = 0;
	}  
  });
EOT;
$this->registerJs($progress); 		


$highlight = <<<EOT
/*
 * jQuery Highlight plugin
 *
 * Based on highlight v3 by Johann Burkard
 * http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
 *
 * Code a little bit refactored and cleaned (in my humble opinion).
 * Most important changes:
 *  - has an option to highlight only entire words (wordsOnly - false by default),
 *  - has an option to be case sensitive (caseSensitive - false by default)
 *  - highlight element tag and class names can be specified in options
 *
 * Usage:
 *   // wrap every occurrance of text 'lorem' in content
 *   // with <span class='highlight'> (default options)
 *   $('#content').highlight('lorem');
 *
 *   // search for and highlight more terms at once
 *   // so you can save some time on traversing DOM
 *   $('#content').highlight(['lorem', 'ipsum']);
 *   $('#content').highlight('lorem ipsum');
 *
 *   // search only for entire word 'lorem'
 *   $('#content').highlight('lorem', { wordsOnly: true });
 *
 *   // don't ignore case during search of term 'lorem'
 *   $('#content').highlight('lorem', { caseSensitive: true });
 *
 *   // wrap every occurrance of term 'ipsum' in content
 *   // with <em class='important'>
 *   $('#content').highlight('ipsum', { element: 'em', className: 'important' });
 *
 *   // remove default highlight
 *   $('#content').unhighlight();
 *
 *   // remove custom highlight
 *   $('#content').unhighlight({ element: 'em', className: 'important' });
 *
 *
 * Copyright (c) 2009 Bartek Szopka
 *
 * Licensed under MIT license.
 *
 */

jQuery.extend({
    highlight: function (node, re, nodeName, className) {
        if (node.nodeType === 3) {
            var match = node.data.match(re);
            if (match) {
                var highlight = document.createElement(nodeName || 'span');
                highlight.className = className || 'highlight';
                var wordNode = node.splitText(match.index);
                wordNode.splitText(match[0].length);
                var wordClone = wordNode.cloneNode(true);
                highlight.appendChild(wordClone);
                wordNode.parentNode.replaceChild(highlight, wordNode);
                return 1; //skip added node in parent
            }
        } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
                !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
                !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
            for (var i = 0; i < node.childNodes.length; i++) {
                i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
            }
        }
        return 0;
    }
});

jQuery.fn.unhighlight = function (options) {
    var settings = { className: 'highlight', element: 'span' };
    jQuery.extend(settings, options);

    return this.find(settings.element + "." + settings.className).each(function () {
        var parent = this.parentNode;
        parent.replaceChild(this.firstChild, this);
        parent.normalize();
    }).end();
};

jQuery.fn.highlight = function (words, options) {
    var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
    jQuery.extend(settings, options);

    if (words.constructor === String) {
        words = [words];
    }
    words = jQuery.grep(words, function(word, i){
      return word != '';
    });
    words = jQuery.map(words, function(word, i) {
      return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    });
    if (words.length == 0) { return this; };

    var flag = settings.caseSensitive ? "" : "i";
    var pattern = "(" + words.join("|") + ")";
    if (settings.wordsOnly) {
        pattern = "\\b" + pattern + "\\b";
    }
    var re = new RegExp(pattern, flag);

    return this.each(function () {
        jQuery.highlight(this, re, settings.element, settings.className);
    });
};
        
EOT;
$this->registerCss('.highlight {font-weight: bold; text-decoration: underline}');
$this->registerJs($highlight); 		
$aJsWords = ArrayHelper::map($words, 'WordID', 'WordText');
foreach ($aJsWords as $key=>$word) 
{
	$this->registerJs( "$('span').highlight('".$word."');");
}	
?>	

</div>
</div>

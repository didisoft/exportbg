<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MentionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mentions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="modalEdit" class="modal remote fade" role="dialog"></div>    

<div class="mentions-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?php
$defaultExportConfig = [
    GridView::HTML => [
        'label' => 'HTML',
        'icon' => 'file-text',
        'iconOptions' => ['class' => 'text-info'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        'alertMsg' => 'The HTML export file will be generated for download.',
        'options' => ['title' => 'Hyper Text Markup Language'],
        'mime' => 'text/html',
        'config' => [
            'cssFile' => 'http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css'
        ]
    ],
GridView::CSV => [
        'label' => 'CSV',
        'icon' => 'file-code-o', 
        'iconOptions' => ['class' => 'text-primary'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        'alertMsg' => 'The CSV export file will be generated for download.',
        'options' => ['title' => 'Comma Separated Values'],
        'mime' => 'application/csv',
        'config' => [
            'colDelimiter' => ",",
            'rowDelimiter' => "\r\n",
        ]
    ],
    GridView::TEXT => [
        'label' => 'Text',
        'icon' => 'file-text-o',
        'iconOptions' => ['class' => 'text-muted'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        'alertMsg' => 'The TEXT export file will be generated for download.',
        'options' => ['title' => 'Tab Delimited Text'],
        'mime' => 'text/plain',
        'config' => [
            'colDelimiter' => "\t",
            'rowDelimiter' => "\r\n",
        ]
    ],
    GridView::EXCEL => [
        'label' => 'Excel',
        'icon' => 'file-excel-o',
        'iconOptions' => ['class' => 'text-success'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        'alertMsg' => 'The EXCEL export file will be generated for download.',
        'options' => ['title' => 'Excel 95+'],
        'mime' => 'application/vnd.ms-excel',
        'config' => [
            'worksheet' => 'ExportWorksheet',
            'cssFile' => ''
        ]
    ]	
]	
?>
	
<?php $form = ActiveForm::begin(); ?>

<div style="float: left;">
<table>
<tr>
	<td>
	<?= Yii::t('app', 'Period') ?>&nbsp;
	</td>
	<td>
	<?php
		echo Html::activeDropDownList( $searchForm, 'period', 
							[TODAY => Yii::t('app', 'Today'),
							YESTERDAY => Yii::t('app', 'Yesterday'),
							DAYS7 => Yii::t('app', 'Last 7 Days'),
							DAYS30 => Yii::t('app', 'Last 30 days'),
							THIS_MONTH => Yii::t('app', 'This month'),
							CUSTOM_PERIOD => Yii::t('app', 'Custom')],
						['class'=>'form-control',
						 'onchange'=>'if (this.value == '.CUSTOM_PERIOD.') {$("#mentionssearchform-from").prop("disabled",false);$("#mentionssearchform-to").prop("disabled",false);$("#mentionssearchform-from").select();} else {$(\'#mentionssearchform-from\').prop("disabled", true);$(\'#mentionssearchform-to\').prop("disabled", true);}'
						]);	
	?>
	</td>
	<td>&nbsp;
	<?= Yii::t('app', 'Word') ?>&nbsp;
	</td>
	<td>
	<?php
		echo Html::activeDropDownList( $searchForm, 'WordID', 
							ArrayHelper::merge(['0'=>'-'], ArrayHelper::map($words, 'WordID', 'WordText')),
						['class'=>'form-control',
						 'onchange'=>'if (this.value == '.CUSTOM_PERIOD.') {$("#mentionssearchform-from").prop("disabled",false);$("#mentionssearchform-to").prop("disabled",false);$("#mentionssearchform-from").select();} else {$(\'#mentionssearchform-from\').prop("disabled", true);$(\'#mentionssearchform-to\').prop("disabled", true);}'
						]);	
	?>
	</td>
	<td>&nbsp;
	<?= Html::submitButton(Yii::t('app', 'Search'), ['class'=>'btn btn-success']) ?>
	</td>
</tr>
<tr>
	<td>
	&nbsp;
	</td>
</tr>
<tr>
	<td align="right"> 
	<?= Yii::t('app', 'From') ?>&nbsp;
	</td>
	<td>
	<?php echo DatePicker::widget([
		'model' => $searchForm, 
		'attribute' => 'from',
		'type' => DatePicker::TYPE_INPUT,
		'pluginOptions' => [
			'autoclose'=>true,
			'format' => 'm/d/yyyy'
		],
		'options' => ($searchForm->period == CUSTOM_PERIOD ? [] : ['disabled'=>'true']),
	]);
	?>
	</td>
	<td align="right">
	&nbsp;
	<?= Yii::t('app', 'To') ?>
	&nbsp;
	</td>
	<td>
	<?php echo DatePicker::widget([
		'model' => $searchForm, 
		'attribute' => 'to',
		'type' => DatePicker::TYPE_INPUT,
		'pluginOptions' => [
			'autoclose'=>true,
			'format' => 'm/d/yyyy'
		],
		'options' => ($searchForm->period == CUSTOM_PERIOD ? [] : ['disabled'=>'true']),		
	]);
	?>
	</td>
	<td>
	&nbsp;
    <?php
		echo Html::activeCheckbox( $searchForm, 'spam', ['label'=>'<span class="fa fa-trash"/> Show Spam']);	
	?>
	</td>
	<td>
    &nbsp;    
	<?php
        echo Html::activeCheckbox( $searchForm, 'favorites', ['label'=>'<span class="fa fa-bookmark"/> Show Favorites']);		
    ?>
	</td>
</tr>
<tr>
	<td>
	&nbsp;
	</td>
</tr>
<tr>
	<td>
	</td>
    <td>
	<?php
		echo Html::activeDropDownList( $searchForm, 'term', 
							['1'=>'Title', '2' => 'Text'],
						['class'=>'form-control']);
	?>        
    </td>
	<td align="right">
        <?= Yii::t('app', 'Like') ?>&nbsp;
	</td>
    <td>
	<?php
		echo Html::activeTextInput( $searchForm, 'keyword', ['class'=>'form-control'] );	
	?>        
    </td>
	<td>&nbsp;
	
	</td>
</tr>
</table>
</div>
<div style="float: right">
    <div class="box box-success box-solid" id="tagcloud" style="display: none">
        <div class="box-header with-border">
          <h3 class="box-title">Tag Cloud</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        </div>
        <!-- /.box-body -->
    </div>    
</div>
<div style="clear: both">
&nbsp;

<?php ActiveForm::end(); ?>	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'pjax' => 'true',
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			[
				'class' => 'kartik\grid\ExpandRowColumn',
				'value' => function ($model, $key, $index, $column) {
					return GridView::ROW_COLLAPSED;
				},
				// Load everything
				'detail' => function ($model, $key, $index, $column) {
					return Yii::$app->controller->renderPartial('view', ['model'=>$model]);
				}, 
				// Load via ajax
				// 'detailUrl' => \yii\helpers\Url::to(['/mentions/details'])
			],			
			'MentionDate:date',
            [
                'header' => Html::a('', '', ['class' => 'fa fa-trash', 'title' => 'Spam']),
                'contentOptions'=>['class'=>'kv-row-select'],
                'content'=>function($model, $key){
                    return Html::checkbox('selection[]', $model->IsSpam, ['class'=>'kv-row-checkbox chk-spam', 'value'=>$key, 
                                            'onclick'=>"$(this).closest('tr').toggleClass('danger');"]);
                },
                'hAlign'=>'center',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
            ],
            [
                'header' => Html::a('', '', ['class' => 'fa fa-bookmark']),
                'contentOptions'=>['class'=>'kv-row-select chk-favorites'],
                'content'=>function($model, $key){
                    return Html::checkbox('selection[]', $model->Favorites, ['class'=>'kv-row-checkbox', 'value'=>$key, 
                                            'onclick'=>"$(this).closest('tr').toggleClass('danger');"]);
                },
                'hAlign'=>'center',
                'hiddenFromExport'=>true,
                'mergeHeader'=>true,
            ],                        
            'word.WordText',
            [
                'label'=>'Title',
                'format' => 'raw',
                'value'=>function ($data) {
                        return Html::a($data['Title'], $data['Link'], ['target' => '_blank']);
                },
            ],                        
			'Snippet',
            //['class' => 'yii\grid\ActionColumn'],
        ],
		'hover' => true,
		'responsive'=> true,
		'export' => [
			'fontAwesome' => true
		],
		'panel' => [
			'type' => GridView::TYPE_PRIMARY,
			//'heading' => $heading,
		],		
		'exportConfig'=>$defaultExportConfig,
		'toolbar'=>[
				'{export}',
				'{toggleData}',
			]		
    ]); ?>
	
</div>

<?php
$highlight = 'var spamUrl = "'. Yii::$app->urlManager->createAbsoluteUrl(["mentions/spam"]) . '";';
$highlight .= 'var favoritesUrl = "'. Yii::$app->urlManager->createAbsoluteUrl(["mentions/favorites"]) . '";';
$highlight .= <<<EOT
/*
 * jQuery Highlight plugin
 *
 * Based on highlight v3 by Johann Burkard
 * http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
 *
 * Code a little bit refactored and cleaned (in my humble opinion).
 * Most important changes:
 *  - has an option to highlight only entire words (wordsOnly - false by default),
 *  - has an option to be case sensitive (caseSensitive - false by default)
 *  - highlight element tag and class names can be specified in options
 *
 * Usage:
 *   // wrap every occurrance of text 'lorem' in content
 *   // with <span class='highlight'> (default options)
 *   $('#content').highlight('lorem');
 *
 *   // search for and highlight more terms at once
 *   // so you can save some time on traversing DOM
 *   $('#content').highlight(['lorem', 'ipsum']);
 *   $('#content').highlight('lorem ipsum');
 *
 *   // search only for entire word 'lorem'
 *   $('#content').highlight('lorem', { wordsOnly: true });
 *
 *   // don't ignore case during search of term 'lorem'
 *   $('#content').highlight('lorem', { caseSensitive: true });
 *
 *   // wrap every occurrance of term 'ipsum' in content
 *   // with <em class='important'>
 *   $('#content').highlight('ipsum', { element: 'em', className: 'important' });
 *
 *   // remove default highlight
 *   $('#content').unhighlight();
 *
 *   // remove custom highlight
 *   $('#content').unhighlight({ element: 'em', className: 'important' });
 *
 *
 * Copyright (c) 2009 Bartek Szopka
 *
 * Licensed under MIT license.
 *
 */

jQuery.extend({
    highlight: function (node, re, nodeName, className) {
        if (node.nodeType === 3) {
            var match = node.data.match(re);
            if (match) {
                var highlight = document.createElement(nodeName || 'span');
                highlight.className = className || 'highlight';
                var wordNode = node.splitText(match.index);
                wordNode.splitText(match[0].length);
                var wordClone = wordNode.cloneNode(true);
                highlight.appendChild(wordClone);
                wordNode.parentNode.replaceChild(highlight, wordNode);
                return 1; //skip added node in parent
            }
        } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
                !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
                !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
            for (var i = 0; i < node.childNodes.length; i++) {
                i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
            }
        }
        return 0;
    }
});

jQuery.fn.unhighlight = function (options) {
    var settings = { className: 'highlight', element: 'span' };
    jQuery.extend(settings, options);

    return this.find(settings.element + "." + settings.className).each(function () {
        var parent = this.parentNode;
        parent.replaceChild(this.firstChild, this);
        parent.normalize();
    }).end();
};

jQuery.fn.highlight = function (words, options) {
    var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
    jQuery.extend(settings, options);

    if (words.constructor === String) {
        words = [words];
    }
    words = jQuery.grep(words, function(word, i){
      return word != '';
    });
    words = jQuery.map(words, function(word, i) {
      return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    });
    if (words.length == 0) { return this; };

    var flag = settings.caseSensitive ? "" : "i";
    var pattern = "(" + words.join("|") + ")";
    if (settings.wordsOnly) {
        pattern = "\\b" + pattern + "\\b";
    }
    var re = new RegExp(pattern, flag);

    return this.each(function () {
        jQuery.highlight(this, re, settings.element, settings.className);
    });
};


$('.chk-spam').change(function(e) {
   var checked = $(this).is(':checked') ? 1 : 0;
   $.ajax(spamUrl, {data: {id: $(this).closest('tr').data('key'), checked: checked}});   
});        
        

$('.chk-favorites').change(function(e) {
   var checked = $(this).is(':checked') ? 1 : 0;
   $.ajax(favoritesUrl, {data: {id: $(this).closest('tr').data('key'), checked: checked}});   
});        
   
EOT;
$this->registerCss('.highlight {font-weight: bold; text-decoration: underline}');
$this->registerJs($highlight); 		
$aJsWords = ArrayHelper::map($words, 'WordID', 'WordText');
foreach ($aJsWords as $key=>$word) 
{
	$this->registerJs( "$('td').highlight('".$word."');");
}	
?>	

<?php
    $this->registerJs($topwordsjs);
?>

<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;
use smallbearsoft\ajax\Ajax;

/* @var $this yii\web\View */
/* @var $model app\models\Mentions */
?>
<div class="mentions-view" style="background-color: #fff">
	
	<table class="table table-striped table-bordered detail-view">
		<tbody>
			<tr>
                <td rowspan="4">
                    <a class="btn btn-info" href="javascript:jQuery.ajax({
                        data:'id=<?= $model->MentionID ?>', 
                        success:function(request){ 
                                $('#modalEdit').html(request); 
                                $('#modalEdit').modal('show');
                        }, 
                        type:'post', 
                        url: '<?= Yii::$app->request->baseUrl ?>/words/filter?id=<?= $model->WordID ?>&mentionId=<?= $model->MentionID ?>'});">
                        <?= Yii::t('app', 'Manually filter') ?>
                    </a>                    
                </td>                
				<th><?= Yii::t('app', 'Title') ?></th>
				<td><?= $model->Title ?></td>                
			</tr>
			<tr>
				<th><?= Yii::t('app', 'Link') ?></th>
				<td><?= Html::a( $model->Link, $model->Link, ['target' => '_blank']) ?></td>
			</tr>
			<tr>
				<th><?= Yii::t('app', 'Snippet') ?></th>
				<td>
                    <?= $model->Snippet ?>
                </td>
			</tr>
			<tr>
				<th><?= Yii::t('app', 'Mention Date') ?></th>
				<td><?= Yii::$app->formatter->asDate($model->MentionDate) ?></td>
			</tr>
		</tbody>
	</table>

</div>

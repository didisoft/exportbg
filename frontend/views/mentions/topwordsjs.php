var stopWords = ['and', 'the', 'for', 'their', 'that','but','or','as','if','when','than','because','while','where','after',
'so','though','since','until','whether','before','although','nor','like','once','unless','now','except'];
function getTopNWords(text, n)
{
    if (!text) return null;

    var wordRegExp = /\w+(?:'\w{1,2})?/g;
    var words = {};
    var matches;
    while ((matches = wordRegExp.exec(text)) != null)
    {
        var word = matches[0].toLowerCase();
		if (word.length < 3) continue;
		if ($.inArray(word, stopWords) > -1) continue;
        if (typeof words[word] == "undefined")
        {
            words[word] = 1;
        }
        else
        {
            words[word]++;
        }
    }

    var wordList = [];
    for (var word in words)
    {
        if (words.hasOwnProperty(word))
        {
            wordList.push([word, words[word]]);
        }
    }
    wordList.sort(function(a, b) { return b[1] - a[1]; });

    var topWords = [];
    for (var i = 0; i < n; i++)
    {
        topWords.push(wordList[i][0]);
    }
    return topWords;
}
var s = ''; $( "td[data-col-seq=6]" ).each( function() {s+= $( this ).text()}); 
var words = getTopNWords(s, 10);

if (words) {
    $('#tagcloud').show();
    $('#tagcloud .box-body').html(words.join(', '))
}

<?php
/* @var $this yii\web\View */
$this->title = Yii::t('app', "Account activation");
?>
<div class="site-index">

    <div class="body-content">

		<h2><?= Yii::t('app', "Wrong activation code") ?></h2>

		<p>
		<b><?= Yii::t('app', "Please contact our support department.") ?></b>
		</p>
	</div>

</div>

<?php
use yii\helpers\Html;
?>
<table cellspacing="0" class="has-feed">
	<tbody>
		<?php $i = 0;
		foreach ($results as $result) { 
			$i++;
		?>
		<tr class="alt">
			<td>
				 <b>
					<?php echo $result['Title'] ?>		
                 </b>
				<br/>
				<span>
					<?php echo str_ireplace($word, '<b>'.$word.'</b>', $result['Snippet']); ?>
				</span>
				<br/>				
				<span>
					<a href="<?php echo $result['Link'] ?>" rel="nofollow" mc:disable-tracking>
					 <?php 
						$url = $result['Link'];
						$parts = parse_url($url);
						echo $parts['scheme'] . '://' . $parts['host'];										
					?>
					</a>
				</span>								
				<br/>
				<span>
                    <b><?php echo Yii::$app->formatter->asDate($result['MentionDate']) ?></b>
				</span>
				<br/>
                <small>
                <?php
                    $spamLink = Yii::$app->urlManager->createAbsoluteUrl(['mentions/spam-email', 'id' =>$result['CustomerID'], 'mid' => $result['MentionID']]);
                    echo Html::a(Html::encode('Skip such results'), $spamLink);
                ?>
				&nbsp;|&nbsp;
                <?php
                    $favLink = Yii::$app->urlManager->createAbsoluteUrl(['mentions/favorites-email', 'id' =>$result['CustomerID'], 'mid' => $result['MentionID']]);
                    echo Html::a(Html::encode('Add to Favorites'), $favLink);
                ?>
                </small>
				<br/>
                <br/>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<form method=post action="<?= EPAY_URL ?>" name="member_signup">

 <input type=hidden name=PAGE value="paylogin">
 <input type=hidden name=ENCODING value="utf-8">
 <input type=hidden name=ENCODED value="<?= $encoded ?>">
 <input type=hidden name=CHECKSUM value="<?= $checksum ?>">
 <input type=hidden name=URL_OK value="<?= Yii::$app->urlManager->createAbsoluteUrl(['billing/epay-success']) ?>">
 <input type=hidden name=URL_CANCEL value="<?= Yii::$app->urlManager->createAbsoluteUrl(['billing/epay-cancel']) ?>">

<script type="text/javascript"> 
window.onload = function(){
  document.forms['member_signup'].submit()
}
</script>
</form>
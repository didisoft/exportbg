<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
					//['label' => Yii::t('app', 'Dashboard'), 'icon' => 'fa  fa-dashboard', 'url' => ['/mentions/index']],
                    //['label' => '', 'options' => ['class' => 'header']],
                    ['label' => Yii::t('app', 'Mentions'), 'icon' => 'fa fa-commenting', 'url' => ['/mentions/index']],
                    ['label' => Yii::t('app', 'Words'), 'icon' => 'fa  fa-y-combinator', 'url' => ['/words/index']],
                    ['label' => Yii::t('app', 'Subscription'), 'icon' => 'fa fa-bank', 'url' => ['/billing/index']],
					['label' => Yii::t('app', 'Settings'), 'icon' => 'fa fa-cogs', 'url' => ['/settings/index']],
					['label' => Yii::t('app', 'Logout'), 'icon' => 'fa fa-power-off', 'url' => ['/site/logout']],
                ],
            ]
        ) ?>

    </section>

</aside>


<?php
/* @var $this yii\web\View */
$this->title = \Yii::$app->params['siteName'];
?>
<div class="box box-primary">
    <!-- form start -->
      <div class="box-body">

		<h2>Welcome to <?= \Yii::$app->params['siteName'] ?></h2>

		<p>
		<b>You still have no search terms.</b>
		</p>
		
		<p>Enter the <?php echo \yii\helpers\Html::a(Yii::t('app', 'Words'), ['words/create']) ?>, that you would like to be tracked.</p>
		<p>
		<?php echo \yii\helpers\Html::a(Yii::t('app', 'Words'). ' &raquo;', ['words/create'], ['class'=>'btn btn-info']) ?>
	</div>

</div>

<?php
/* @var $this yii\web\View */
$this->title = 'Account activation';
?>
<div class="site-index">

    <div class="body-content">

        <p>
            Thank you for signing up to <?= \Yii::$app->params['siteName'] ?>.
        </p>
        <p>
            We will never share your email with anyone, and you may unsubscribe at any time.
        </p>
        <p>
            There is just one more step.  We sent you a confirmation email with a link to activate your account.  
            Please check your email and click the link.
        </p>
	</div>

</div>

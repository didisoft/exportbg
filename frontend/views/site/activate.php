<?php
/* @var $this yii\web\View */
$this->title = 'Account activated';
?>
<div class="site-index">

    <div class="body-content">

		<?php if ($activated) { ?>
		<h2><?= Yii::t('app', "You have successfully activated your registration in ") ?> <?= \Yii::$app->params['siteName'] ?></h2>

		<p>
		<b><?= Yii::t('app', "You can now access your Control Dashboard") ?></b>
		</p>
		
		<?php echo \yii\helpers\Html::a(Yii::t('app', 'Login'). ' &raquo;', ['site/login'], ['class'=>'btn btn-info']) ?>
		<?php } else { ?>
		<h2><?= Yii::t('app', "Wrong activation code") ?></h2>

		<p>
		<b><?= Yii::t('app', "Please try again or contact our support department.") ?></b>
		</p>
		
		<?php } ?>
	</div>

</div>

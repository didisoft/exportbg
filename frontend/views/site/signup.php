<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\authclient\widgets\AuthChoice;

$this->title = Yii::t('app', 'Registration');
$this->params['breadcrumbs'][] = $this->title;

$css = 
".auth-icon.facebook {
    color: #ffffff;
    background-color: #395697;
    line-height: 32px;
    padding: 0 40px;
    margin-left: 10px;
    width: 136px;
    float: left;
}
.auth-icon.google {
    color: #ffffff;
    background-color: #e0492f;
    line-height: 32px;
    padding: 0 40px;
    width: 136px;
    float: left;
}

.auth-iconx {
}";

$this->registerCss($css);
$this->registerCssFile(Yii::$app->request->baseUrl.'/js/bootstrap-social.css'); 
?>  
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
<div>
                
<?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/reg']]); ?>
<?php foreach ($authAuthChoice->getClients() as $client):
    if ($client->getName() == 'google') {
        $client->returnUrl = Yii::$app->params['siteUrl'] . '/site/reg?authclient=google';
    }
    echo $authAuthChoice->clientLink($client, Html::tag('span', $client->getTitle(), ['class' => 'auth-icon ' . $client->getName()]));
endforeach; ?>
<?php AuthChoice::end(); ?>                
            
            </div>			            
    <div style="clear: both"></div>
    <hr/>

    <div class="row">            
		<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <div class="col-lg-5">
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
				<?php $field = $form->field($model, 'terms');
					$field->parts['{label}'] = Yii::t('app', 'Terms');
					echo $field->checkbox([], false);
				?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Continue'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
				
        </div>
        <?php ActiveForm::end(); ?>		
    </div>
</div>

<div style="border: 1px solid #dddddd;padding: 10px; margin-top: 20px;">
    <?= Yii::t('app','Already have an account?') ?> <?= Html::a(Yii::t('app','Login'), ['site/login']) ?>
</div>

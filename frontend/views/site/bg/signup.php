<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = Yii::t('app', 'Registration');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">            
		<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <div class="col-lg-5">
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
				<?php $field = $form->field($model, 'terms');
					$field->parts['{label}'] = Yii::t('app', 'Terms');
					echo $field->checkbox([], false);
				?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Continue'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
				
        </div>
        <?php ActiveForm::end(); ?>		
    </div>
</div>

<h3>Мога ли да променя плана си след време?</h3>
<p>
Да, разбира се. eReport.bg е месечна услуга, по всяко време можете да преминете на по-висок или по-нисък план, както и да се откажете от услугата.
</p>

<h3>Какви начини на разплащане приемате?</h3>
<p>
eReport.bg приема:<br/>
- Български дебитни карти през ePay.bg<br/>
- Плащане по Банков път към Първа Инвестиционна Банка
</p>

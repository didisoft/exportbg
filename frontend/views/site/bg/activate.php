<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

		<?php if ($activated) { ?>
		<h2>Успешно активирахте вашата регистрация в <?= \Yii::$app->params['siteName'] ?></h2>

		<p>
		<b>Заповядайте в своя контролен панел</b>
		</p>
		
		<?php echo \yii\helpers\Html::a(Yii::t('app', 'Login'). ' &raquo;', ['site/login'], ['class'=>'btn btn-info']) ?>
		<?php } else { ?>
		<h2>Невалиден код за активация</h2>

		<p>
		<b>Моля опитайте отново.</b>
		</p>
		
		<?php } ?>
	</div>

</div>

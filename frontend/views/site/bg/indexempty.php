<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

		<h2>Добре дошли в системата на eReport.BG</h2>

		<p>
		<b>Все още нямате думи/фрази за търсене.</b>
		</p>
		
		<p>За да започнете работа със системата, <?php echo \yii\helpers\Html::a('въведете думи/фрази', ['words/create']) ?>, които искате да следите 
			и ще започнете да получавате известия по електронната поща при тяхното публикуване в Интернет</p>
		<p>
		<?php echo \yii\helpers\Html::a(Yii::t('app', 'Words'). ' &raquo;', ['words/create'], ['class'=>'btn btn-info']) ?>
	</div>

</div>

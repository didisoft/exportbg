<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

		<h2>Добре дошли в системата на eReport.BG</h2>

		<p>За да започнете работа със системата, въведете думи или фрази, които искате да следите 
			и автоматично ще започнете да получавате всекидневни известия по електронната поща за тяхното публикуване в Интернет</p>
		<p>
		<?php echo \yii\helpers\Html::a(Yii::t('app', 'Words'). ' &raquo;', ['words/index'], ['class'=>'btn btn-default']) ?>
	</div>

</div>

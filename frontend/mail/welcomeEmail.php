<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = \Yii::$app->params['siteName'] . ' Welcome';	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Welcome to <?= \Yii::$app->params['siteName'] ?></p>

	<p>Your registration is now active. You can access your account by going to<br/>
	
    <?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
	
	<p>
	If you get stuck or need help, please don't hesitate to drop us a line.
	</p>
	
Best Regards,
<br/>
<?php echo \Yii::$app->params['siteName'] ?> Support Team
	
</div>
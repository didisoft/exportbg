<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = '[' . \Yii::$app->params['siteName'] . '] Activate Your Registration';	

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate', 'id' =>$user->LoginID, 'token' => $user->auth_key]);
?>
<div class="password-reset">
    <p>Hi,</p>

    <p>Welcome to <?= \Yii::$app->params['siteName'] ?></p>
    
    <p>
    Your login information is:<br/>
    <br/>
        Username: <?= $user->Email ?><br/>
        Password: <?= $password ?><br/>
    </p>    

	<p>Please follow the link below in order to activate your registration</p>
	
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>

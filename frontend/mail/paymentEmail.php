<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = '[' . \Yii::$app->params['siteName'] . '] Payment Confirmation';

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['billing/invoice-external', 'id' => $id, 'key' => $key]);
?>
<div class="password-reset">
    <p>Hello,</p>

    <p>Thank your for the payment for the services of <?= \Yii::$app->params['siteName'] ?>
	<br/>
	Your subscription plan "<?= $plan ?>" has been activated till <?= $expiration ?>.
	</p>
	
<br/>
<br/>
Best Regards,
<br/>
<?php echo \Yii::$app->params['siteName'] ?> Support Team
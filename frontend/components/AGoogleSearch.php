<?php
namespace frontend\components;

//use \Google\Client;
//use \Google\Service\Customsearch;
//Yii::import('application.components.Google.Service.*');

class AGoogleSearch {

	public $service = null;
	public $client = null;
	
	function AGoogleSearch() {
		$this->client = new \Google_Client();//Client();
		$this->client->setApplicationName("forumcastle-504");

		$apiKey = "AIzaSyCE_He037kHvNyanwat5I7PuwNAUrQvGBU";
		$this->client->setDeveloperKey($apiKey);
		$this->service = new \Google_Service_Customsearch_Search($this->client); // Search
	}
	
	function GetSourceId() {
		return JOB_ID_Google;
	}
	
	function GetName() {
		return "Google";
	}
	
	function Search($word) {
		$days = 1;
		$results = $this->GetResultsFor($this->service, $word, $days);
		
		$newResuts = array();
		foreach ($results as $result) 
		{
			$newResut = new ASearchResult($this->GetSourceId());
			$newResut->title = $result->getTitle();
			$newResut->link = $result->getLink();
			$newResut->snippet = $result->getSnippet();
			$newResuts[] = $newResut;
		}
		return $newResuts;
	}	

	function SearchTest() {
		$out = "<h3>Results for pgp</h3>";
		$results = $this->GetResultsFor($this->service, 'pgp', 1);
		$out .= $this->FormatResults($results);
		echo $out;
	}	

	function GetResultsFor($service, $word, $days=1) 
	{ 
		/************************************************
		  We make a call to our service, which will
		  normally map to the structure of the API.
		  In this case $service is Books API, the
		  resource is volumes, and the method is
		  listVolumes. We pass it a required parameters
		  (the query)	, and an array of named optional
		  parameters.
		 ************************************************/
		 // Google_Service_Customsearch_Search
		// Google_Service_Customsearch_Cse_Resource
		$cse = $service->cse; 

		$list = $cse->listCse($word, 
				array('cx'=>'011316330735753128977:_kqlh5___w0',
						// d[number]: requests results from the specified number of past days.
						// w[number]: requests results from the specified number of past weeks.
						// m[number]: requests results from the specified number of past months.
						// y[number]: requests results from the specified number of past years.
						'dateRestrict'=>'d'. $days) 
			);
		// Google_Service_Customsearch_Result
		return $list->getItems();
	}	
	
	function FormatResults($results)
	{
		$out = '';
		foreach ($results as $item) 
		{
			$out .= '<br>';	
			$out .= '<b>' .$item->getTitle() . '</b>';
			$out .= '<br>';
			$out .= '<a href="' . $item->getLink() . '">' .$item->getLink() . '</a>';
			$out .= '<p>' . $item->getSnippet() . '</p>';
		}
		return $out;
	}	
		
}
?>
<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Mentions;

/**
 * MentionsSearch represents the model behind the search form about `app\models\Mentions`.
 */
class MentionsSearch extends Mentions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MentionID', 'CustomerID', 'WordID', 'SourceID', 'JobID'], 'integer'],
            [['Title', 'Link', 'Snippet', 'MentionDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        
        $query = Mentions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->query->andFilterWhere(['CustomerID' => Yii::$app->user->identity->CustomerID]);		
        
		$searchForm = new MentionsSearchForm();
		
        //$dataProvider = new ActiveDataProvider(['query' => Mentions::find()]);
		//$dataProvider->query->andFilterWhere(['CustomerID' => Yii::$app->user->identity->CustomerID]);		
		
		if (Yii::$app->request->isPost) {			
			$searchForm->attributes = Yii::$app->request->post('MentionsSearchForm');
			Yii::$app->session->open();
			Yii::$app->session['MentionsSearchForm'] = serialize($searchForm);
			Yii::$app->session->close();
		} else {
			if (isset(Yii::$app->session['MentionsSearchForm'])) {
				$searchForm = unserialize(Yii::$app->session['MentionsSearchForm']);
			}
		}
        
		if ($searchForm->WordID > 0) {
			$dataProvider->query->andWhere(
				'WordID = '.$searchForm->WordID);
		}
		
		if ($searchForm->period == TODAY) {
			$dataProvider->query->andWhere(
				'DATE(`MentionDate`) = DATE(NOW())');
		}
		else if ($searchForm->period == YESTERDAY) {
			$dataProvider->query->andWhere(
				'Date(MentionDate) = subdate(current_date, 1)');
		}
		else if ($searchForm->period == DAYS7) {
			$dataProvider->query->andWhere(
				'Date(MentionDate) > subdate(current_date, 7)');
		}
		else if ($searchForm->period == DAYS30) {
			$dataProvider->query->andWhere(
				'Date(MentionDate) > subdate(current_date, 30)');
		}
		else if ($searchForm->period == THIS_MONTH) {
			$dataProvider->query->andWhere(
				'YEAR(MentionDate) = YEAR(current_date)');
			$dataProvider->query->andWhere(
				'MONTH(MentionDate) = MONTH(current_date)');
		}
		else if ($searchForm->period == CUSTOM_PERIOD) {				
			if (empty($searchForm->to) && !empty($searchForm->from)) {
				$dataProvider->query->andWhere(						
						'MentionDate >= \'' . \DateTime::createFromFormat('d/m/yy', $searchForm->from)->format('Y-m-d') . '\''
				);
			} else if (empty($searchForm->from) && !empty($searchForm->to)) {
				$dataProvider->query->andWhere(						
						'MentionDate < \'' . \DateTime::createFromFormat('d/m/yy', $searchForm->to)->format('Y-m-d') . '\''
				);					
			} else if (empty($searchForm->from) && empty($searchForm->to)) {
				$dataProvider->query->andWhere(
									'DATE(`MentionDate`) = DATE(NOW())');						
			} else {
				if (\DateTime::createFromFormat('d/m/yy', $searchForm->from) > 
					\DateTime::createFromFormat('d/m/yy', $searchForm->to)) {
					$tmp = $searchForm->from;
					$searchForm->from = $searchForm->to;
					$searchForm->to = $tmp;
				}
			
				$dataProvider->query->andFilterWhere(
					['between', 
						'MentionDate', 
						\DateTime::createFromFormat('m/d/yy', $searchForm->from)->format('Y-m-d'), //Yii::$app->formatter->asDate(\DateTime::createFromFormat('d/m/y', $searchForm->from), 'short'),
						\DateTime::createFromFormat('m/d/yy', $searchForm->to)->format('Y-m-d') //Yii::$app->formatter->asDate(\DateTime::createFromFormat('d/m/y', $searchForm->to), 'short')
					]
				); 
			}
		} else {
				// by default show results for today
				$dataProvider->query->andWhere(
					'DATE(`MentionDate`) = DATE(NOW())');		
		}

        if ($searchForm->spam) {
            $dataProvider->query->andWhere(
                'IsSpam > 0');
        } else {
            $dataProvider->query->andWhere(
                'IsSpam = 0');            
        }
        
		if ($searchForm->favorites > 0) {
			$dataProvider->query->andWhere(
				'Favorites = 1');
		}

        if ($searchForm->keyword) {
            if ($searchForm->term == MentionsSearchForm::TITLE) {
                $dataProvider->query->andWhere(
                    ['like', 'Title', $searchForm->keyword]);
            } else if ($searchForm->term == MentionsSearchForm::SNIPPET) {
                $dataProvider->query->andWhere(
                    ['like', 'Snippet', $searchForm->keyword]);
            }
        }

        if ($this->Title) {
            $query->andFilterWhere(['like', 'Title', $this->Title]);
        }
        if ($this->Link) {
            $query->andFilterWhere(['like', 'Title', $this->Link]);
        }
        if ($this->Snippet) {
            $query->andFilterWhere(['like', 'Title', $this->Snippet]);
        }

        //echo $query->createCommand()->getRawSql();
        //Yii::$app->end();
        
        return $dataProvider;
    }
}

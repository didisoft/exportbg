<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "mentions".
 *
 * @property integer $MentionID
 * @property integer $CustomerID
 * @property integer $WordID
 * @property integer $SourceID
 * @property integer $JobID
 * @property string $Title
 * @property string $Link
 * @property string $Snippet
 * @property string $MentionDate
 */
class Mentions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mentions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerID', 'WordID', 'SourceID', 'JobID', 'Title', 'Link', 'MentionDate'], 'required'],
            [['CustomerID', 'WordID', 'SourceID', 'JobID', 'Favorites', 'IsSpam'], 'integer'],
            [['MentionDate', 'Favorites'], 'safe'],
            [['Title', 'Link'], 'string', 'max' => 5000],
            [['Snippet'], 'string', 'max' => 5000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'MentionID' => 'Mention ID',
            'CustomerID' => 'Customer ID',
            'WordID' => 'Word ID',
            'SourceID' => 'Source ID',
            'JobID' => 'Job ID',
            'Title' => Yii::t('app', 'Title'),
            'Link' => Yii::t('app', 'Link'),
            'Snippet' => Yii::t('app', 'Snippet'),
            'MentionDate' => Yii::t('app', 'Mention Date'),
        ];
    }
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(\common\models\Words::className(), ['WordID' => 'WordID']);
    }	
}

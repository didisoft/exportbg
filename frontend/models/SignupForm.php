<?php
namespace frontend\models;

use common\models\User;
use common\models\Customers;
use common\models\Plans;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
	public $terms;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This Email is already registered!'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
			
            ['terms', 'required', 'requiredValue' => 1, 'message' => Yii::t('app', 'Please accept our Terms and Conditions statement')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
			'terms' => Yii::t('app', 'Terms'),
        ];
    }
	
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {	
        if ($this->validate()) {
			$trialPlan = Plans::findOne(['slug' => 'trial']);	
		
			$customer = new Customers();
			$customer->Active = 0;
			$customer->PlanID = $trialPlan->PlanID;
			$customer->RegistrationTime = new \yii\db\Expression('NOW()');
			$customer->PlanExpirationTime = new \yii\db\Expression('date_add( NOW(), interval 15 day)');
			$customer->PlanActive = 1;
			$customer->activkey = sha1(microtime().$this->password);
			$customer->Discount = 0;

            if ($customer->save(false)) {			
				$user = new User();
				$user->CustomerID = $customer->getPrimaryKey();
				$user->Email = $this->email;
				$user->setPassword($this->password);
				$user->generateAuthKey();
				$user->IsAdmin = 1;
				if (!$user->save(false)) {
                    $this->error( var_export($user->getFirstErrors(), true) );
                }
				return $user;
			} else {
                $this->trace('error saving User');
                $this->error( var_export($customer->getFirstErrors(), true) );
            }
        } else {
            Yii::getLogger()->log(var_export($this->getFirstErrors(), true),
                                \yii\log\Logger::LEVEL_ERROR,
                                'app');            
        }

        return null;
    }
}

<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
e-mail martin_kapkov@yahoo.com телефон за контакт 0876464009
049061 - 8 - 6  -> 1000
006055 - 16 
041066 - 4 
007056 - 4
031241 - 0,6

 * MentionsSearchForm
 */
class MentionsSearchForm extends Model
{
    public $from;
    public $to;
    public $WordID;
    public $period;
    public $customPeriod;
    
    public $spam = 0;
    public $favorites;
    public $keyword;
    public $term;

    const TITLE = 1;
    const SNIPPET = 2;
    const LINK = 3;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['from', 'to', 'WordID', 'period', 'customPeriod', 'spam', 'favorites', 'keyword', 'term'], 'safe'],
			[['from', 'to'], 'date', 'format' => 'd/m/yy'],
            // name, email, subject and body are required
            //[['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            //['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }
}

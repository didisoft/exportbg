<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
	'name' => $params['name'],
    'basePath' => dirname(__DIR__),
	'language' => 'en',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
	'aliases' => [
		'@Google' => '@vendor/google/apiclient/src',
    ],	
	'modules'=> [
		'gridview' =>  [
				'class' => '\kartik\grid\Module'
				// enter optional module parameters below - only if you need to  
				// use your own export download action or custom translation 
				// message source
				// 'downloadAction' => 'gridview/export/download',
				// 'i18n' => []
			],				
	],
	
    'components' => [
        'fastspring' => [
            'class' => 'frontend\components\FastSpring',
            'test_mode' => true,
            'store_id' => $params['fastspring_store'],
            'api_username' => $params['fastspring_user'],
            'api_password' => $params['fastspring_pass'],
            'test_mode' => $params['fastspring_test'],
            'activateKey' => $params['fastspring_private_key_activate'],
            'deactivateKey' => $params['fastspring_private_key_deactivate']
        ],
		'authClientCollection' => [
				'class' => 'yii\authclient\Collection',
				'clients' => [
                    'google' => [
                                    'class' => 'yii\authclient\clients\GoogleOAuth',
                                    'clientId' => $params['google_clientId'],
                                    'clientSecret' => $params['google_clientSecret'],
                                 ],                    
					'facebook' => [
									'class' => 'yii\authclient\clients\Facebook',
									'clientId' => $params['facebook_clientId'],
									'clientSecret' => $params['facebook_clientSecret'],
                                    'attributeNames' => ['name', 'email', 'first_name', 'last_name', 'picture'],
								],					
				],
		],		
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
		'i18n' => [
			'translations' => [
				'app*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages',
					'fileMap' => [
						'yii' => 'yii.php',
					],
					//'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation']
				],
				'yii*' => [
					'class' => 'yii\i18n\PhpMessageSource',
								'sourceLanguage' => 'en',
								'basePath' => '@yii/messages'				
				],												
			],
		],		
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

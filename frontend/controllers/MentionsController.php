<?php

namespace frontend\controllers;

use Yii;
use common\models\Words;
use common\models\MentionsSpam;
use common\controllers\MyController;
use frontend\models\Mentions;
use frontend\models\MentionsSearch;
use frontend\models\MentionsSearchForm;

use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

define( 'CUSTOM_PERIOD', '-1');
define( 'TODAY', '0');
define( 'YESTERDAY', '1');
define( 'DAYS7', '7');
define( 'DAYS30', '30');
define( 'THIS_MONTH', '2');

/**
 * MentionsController implements the CRUD actions for Mentions model.
 */
class MentionsController extends MyController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['spam-email', 'favorites-email'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    
    /**
     * Lists all Mentions models.
     * @return mixed
     */
    public function actionIndex()
    {	
		$words = Words::findAll(['Active'=>1, 'CustomerID'=>Yii::$app->user->identity->CustomerID]);  	
									
		$wordsCount = Words::find()->where(['Active'=>1, 'CustomerID'=>Yii::$app->user->identity->CustomerID])
									->count();  
		if ($wordsCount == 0) {
			return $this->render('/site/indexempty');
		}		
	
        //$searchModel = new MentionsSearch();
		$searchForm = new MentionsSearchForm();
		
        //$dataProvider = new ActiveDataProvider(['query' => Mentions::find()]);
		//$dataProvider->query->andFilterWhere(['CustomerID' => Yii::$app->user->identity->CustomerID]);		
		
		if (Yii::$app->request->isPost) {			
			$searchForm->attributes = Yii::$app->request->post('MentionsSearchForm');
			Yii::$app->session->open();
			Yii::$app->session['MentionsSearchForm'] = serialize($searchForm);
			Yii::$app->session->close();
		} else {
			if (isset(Yii::$app->session['MentionsSearchForm'])) {
				$searchForm = unserialize(Yii::$app->session['MentionsSearchForm']);
			}
		}
		
		$customer = Yii::$app->user->identity->getCustomer();
		if ($customer->PlanActive < 1) {
			Yii::$app->getSession()->setFlash('warning', Yii::t('app', 'Your Subscription plan has expired'));		
		}
		
        $topwordsjs = $this->renderPartial('topwordsjs');
        
        $searchModel = new MentionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
		$countQuery = clone $dataProvider->query;
		$pages = new Pagination(['defaultPageSize' =>10, 'totalCount' => $countQuery->count()]);

		$this->event(Yii::$app->user->identity->CustomerID, EVENT_SEARCH);
		
		//$dataProvider->setPagination(['pageSize' => 5]);
		//echo $dataProvider->query->createCommand()->sql;
		$models = $dataProvider->query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();
		
        return $this->render('index', [
			'searchForm' => $searchForm,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'models' => $models,
			'pages' => $pages,
			'words' => $words,
            'topwordsjs' => $topwordsjs
        ]);
    }

    /**
     * Displays a single Mentions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    
    public function actionDetails()
    {
		$id = Yii::$app->request->post('expandRowKey');
        return $this->renderPartial('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    
	public function actionFavorites($id, $checked) {
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	 	 
			$model = $this->findModel($id);
			$model->Favorites = ($model->Favorites+1)%2;
			$model->save();
			
			$res = array(
				'success' => true,
			);
	 
			return $res;
		}
	}
	
    
    
    public function actionFavoritesEmail($id, $mid) {
        $model = Mentions::findOne(['MentionID' => $mid, 'CustomerID'=>$id]);
        if ($model != null) {
            $model->Favorites = 1;
            $model->save(false);
        }
        
        $this->layout = '//main-login';        
        return $this->render('email-favorites');
    }

    
    
    public function actionSpamEmail($id, $mid) {
        $model = Mentions::findOne(['MentionID' => $mid, 'CustomerID'=>$id]);
        if ($model != null) {
            $model->IsSpam = 2;
            $model->save(false);
        }

        $this->layout = '//main-login';        
        return $this->render('email-spam');
    }

    
    
    public function actionSpam($id, $checked) {
        //IsSpam        
        $model = $this->findModel($id);
        if ($model != null) {
            $model->IsSpam = ($checked? 2 : 0);
            $model->save(false);            
        }
        
        return Yii::t('app', 'Marked as Spam');
    }
    
    
    
    /**
     * Creates a new Mentions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mentions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->MentionID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->actionIndex();	
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Deletes an existing Mentions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
         */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mentions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mentions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mentions::findOne(['MentionID' => $id, 'CustomerID'=>Yii::$app->user->identity->CustomerID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }        
}

<?php

namespace frontend\controllers;

use Yii;
use common\models\Words;
use common\models\WordsSearch;
use common\controllers\MyController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * WordsController implements the CRUD actions for Words model.
 */
class WordsController extends MyController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Words models.
     * @return mixed
     */
    public function actionIndex()
    {
		$plan = Yii::$app->user->identity->getCustomer()->getPlan();
	
        $searchModel = new WordsSearch();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Words::find()->andFilterWhere(['Active' => 1, 'CustomerID'=>Yii::$app->user->identity->CustomerID]),
        ]);
		//$searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->identity->CustomerID);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'plan' => $plan
        ]);
    }

    /**
     * Displays a single Words model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Words model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Words();

        if ($model->load(Yii::$app->request->post())) {
			$connection = \Yii::$app->db;		
			$connection->createCommand()
						->insert($model->tableName(), [
								'EnteredBy' => Yii::$app->user->identity->LoginID,
								'EnteredOn' => new \yii\db\Expression('NOW()'),
								'CustomerID' => Yii::$app->user->identity->CustomerID,
								'WordText' => $model->WordText,
								'ExactMatch' => $model->ExactMatch,
								'OrTerms' => $model->OrTerms,
								'Active' => 1,
								'ExcludeTerms' => $model->ExcludeTerms,
							])
						->execute();		
						
			$this->event(Yii::$app->user->identity->CustomerID, EVENT_ADD_WORD, $model->WordText);
			
			//$model->CustomerID = Yii::$app->user->identity->CustomerID;
			//$model->EnteredBy = Yii::$app->user->identity->LoginID;
			//$model->EnteredOn = new \yii\db\Expression('NOW()');			
			//$model->save(false);
            return $this->actionIndex();
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Words model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
		
			$this->event(Yii::$app->user->identity->CustomerID, EVENT_CHANGE_WORD, $model->WordText);
			
            if (\Yii::$app->request->isAjax) {
                return 'success';
            } else {
                return $this->actionIndex();
            }    
        } else {
            if (\Yii::$app->request->isAjax) {
                return $this->renderAjax('_form_ajax', [
                    'model' => $model,
                ]);                
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Words model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionFilter($id, $mentionId)
    {
        $model = $this->findModel($id);
        $metntion = \frontend\models\Mentions::findOne(['MentionID' => $mentionId]);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
		
            if (\Yii::$app->request->isAjax) {
                return 'success';
            } else {
                return $this->actionIndex();
            }    
        } else {
            if (\Yii::$app->request->isAjax) {
                return $this->renderAjax('_form_ajax_filter', [
                    'model' => $model,
                    'mention' => $metntion,
                ]);                
            } else {
                return $this->render('_form_filter', [
                    'model' => $model,
                    'mention' => $metntion,
                ]);
            }
        }
    }
    
    /**
     * Deletes an existing Words model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
		$model = $this->findModel($id);
		$model->Active = 0;
		$model->save();

		$this->event(Yii::$app->user->identity->CustomerID, EVENT_DELETE_WORD, $model->WordText);
		
        return $this->redirect(['index']);
    }

    /**
     * Finds the Words model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Words the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Words::findOne(['WordID'=>$id, 'CustomerID'=>Yii::$app->user->identity->CustomerID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

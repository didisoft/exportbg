<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\controllers\MyController;
use common\models\Customers;
use common\models\Logins;
use common\models\Sources;
use common\models\MentionsJobs;
use common\models\Words;
use frontend\models\ASearchResult;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;

class A2Controller extends MyController
{

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionSearch($word, $orTerms, $exactTerms, $excludeTerms, $lang, $first=0)
	{
		FacebookSession::setDefaultApplication(\Yii::$app->params['facebook_ap_id'], \Yii::$app->params['facebook_secret']);

		// If you're making app-level requests:
		$session = FacebookSession::newAppSession();

		// To validate the session:
		try {
		  $session->validate();
		} catch (FacebookRequestException $ex) {
		  // Session not valid, Graph API returned an exception with the reason.
		  $this->error( $ex->getMessage() . ' [ ' . $ex->getCode() . ' ]' . $ex->getFile() );
		} catch (\Exception $ex) {
		  // Graph API returned info, but it may mismatch the current app or have expired.
		  $this->error( $ex->getMessage() . ' [ ' . $ex->getCode() . ' ]' . $ex->getFile() );
		}	
					
		// https://developers.facebook.com/docs/graph-api/using-graph-api/v2.3			
		// Searches across page and place objects requires an app access token.
		$params = array(
		  'q' => $word,
		  'locale' => 'bg_BG',
		  'type' => 'page', // see there : Searching
		  'since' => strtotime('yesterday'),  // see there: Time-based Pagination
		);
		if ($first) {
			$params = array(
			  'q' => $word,
			  'type' => 'page', // see there : Searching
			);
		}
		
		// You can chain methods together and get a strongly typed GraphUser
		try {
			$response = (new FacebookRequest(
			  $session, 'GET', '/search', $params
			))->execute();

			$pages = $response->getGraphObjectList('Facebook\GraphPage');		
						
			if (count($pages) > 0) {					
				$newResuts = array();
				foreach ($pages as $page) {
					$response = (new FacebookRequest(
					  $session, 'GET', '/'.$page->getId() //,['metadata'=>1]
					))->execute();
					
					$responseData = $response->getResponse();
		
					$newResut = new ASearchResult();
					$newResut->sourceId = $this->GetSourceId();
					
					if (property_exists($responseData, "name")) {
						$newResut->title = $responseData->name;
						$this->trace('Found FB title : ' . $responseData->name);
					}
					
					if (property_exists($responseData, "link")) {
						$newResut->url = $responseData->link;
						$this->trace('Found FB link : ' . $responseData->name);
					}
					
					if (property_exists($responseData, "description")) {
						$newResut->snippet = mb_substr($responseData->description, 0, 2000);
					} else if (property_exists($responseData, "about")) {
						$newResut->snippet = mb_substr($responseData->about, 0, 2000);
					} // fill empty snippet
					if (count($newResut->snippet) == 0) {
						$newResut->snippet = '-';
					}
										
					$newResuts[] = $newResut;
				}
				
				return $newResuts;				
			}
		} catch (\Exception $ex) {
			// Graph API returned info, but it may mismatch the current app or have expired.
			$this->error( $ex->getMessage() . ' [' . $ex->getCode() . '] - ' . $ex->getFile() . ':' . $ex->getLine() );
		}			
		
		// no results
		return [];
	}
	
	function GetSourceId() {
		return JOB_ID_Facebook;
	}
	
	function GetName() {
		return "Facebook";
	}
	
	function SearchTest() {
		FacebookSession::setDefaultApplication('388009698050632', 'e37ae3cd85955cd56fdace566de5ce64');

		// If you're making app-level requests:
		$session = FacebookSession::newAppSession();

		// To validate the session:
		try {
		  $session->validate();
		} catch (FacebookRequestException $ex) {
		  // Session not valid, Graph API returned an exception with the reason.
		  echo $ex->getMessage();
		} catch (\Exception $ex) {
		  // Graph API returned info, but it may mismatch the current app or have expired.
		  echo $ex->getMessage();
		}	
					
		// https://developers.facebook.com/docs/graph-api/using-graph-api/v2.3			
		// Searches across page and place objects requires an app access token.
		$params = array(
		  'q' => 'Didi Softwares',
		  'type' => 'page', // see there : Searching
		  'since' => '1364849754',  // see there: Time-based Pagination
		);
		
		// You can chain methods together and get a strongly typed GraphUser
		try {
			$response = (new FacebookRequest(
			  $session, 'GET', '/search', $params
			))->execute();
			
			$pages = $response->getGraphObjectList('Facebook\GraphPage');		
			
			if (count($pages) > 0) {
				foreach ($pages as $page) {
					$response = (new FacebookRequest(
					  $session, 'GET', '/'.$page->getId() //,['metadata'=>1]
					))->execute();
					
					$responseData = $response->getResponse();
										
					if (property_exists($responseData, "description")) {
						echo $responseData->description;
						echo '<br>';
					} else if (property_exists($responseData, "about")) {
						echo $responseData->about;
						echo '<br>';
					}
					if (property_exists($responseData, "link")) {
						echo $responseData->link;
						echo '<br>';
					}
					if (property_exists($responseData, "name")) {
						echo $responseData->name;
						echo '<br>';
					}
					echo '<br>';
					echo '<br>';
				}	
				
				//echo var_dump($response->getResponse());
			}
		} catch (\Exception $ex) {
		  // Graph API returned info, but it may mismatch the current app or have expired.
		  echo $ex->getMessage();
		  echo $ex->getCode();
		}			
	}	
}
<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
	
use common\controllers\MyController;
use common\models\Customers;
use common\models\Plans;
use common\models\Orders;
use frontend\models\SignupForm;
use common\models\FastspringOrderDetails;

define('CB_DEV_KEY', 'DEV-6MBCO7V5A4GQN1LOA6JNN7TNDVQTATQ5');
define('CB_CLERK_KEY', 'API-DG5Q91MPNHUNUT9H10LAHLCHKH1G3MHV');

class ClickbankController extends MyController
{    
	// for IPN
	public $enableCsrfValidation = false;
	
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['activatex', 'thankyou', 'order-new'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }



    /**
     * Lists all Words models.
     * @return mixed
     */
    public function actionIndex()
    {
		return $this->actionPlans();
    }

	
	
    public function actionPlan()
    {
		$customer = Yii::$app->user->identity->getCustomer();
		
		$plan1 = $this->findPlanBySlug('plan1');
		$plan2 = $this->findPlanBySlug('plan2');
		$plan3 = $this->findPlanBySlug('plan3');
		$plan4 = $this->findPlanBySlug('plan4');
	
		return $this->render('plans', [
			'model' => Yii::$app->user->identity,
			'customer' => $customer,
			'plan1' => $plan1,
			'plan2' => $plan2,
			'plan3' => $plan3,
			'plan4' => $plan4,
		]);
    }
	
	

	public function actionOrderNew($id) {
		$plan = $this->findPlanBySlug($id);
		$order = new Orders();
				
		// insert into Orders		
		$order->OrderDate = new \yii\db\Expression('NOW()');
		$order->CustomerID = 0;
		$order->PlanID = $plan->PlanID;
		$order->Period = 1;
		$order->Amount = $plan->Price;
		$order->PaymentMethodID = PAYMENT_METHOD_CLICKBANK;
		$order->OrderStatusID = ORDER_PENDING;
				
		if ($order->save()) {
            return $this->redirect('http://' . $plan->FastSpringProduct . '.listen6.pay.clickbank.net?custom1=' . $order->getPrimaryKey());
		} else {
			Yii::$app->getSession()->setFlash('error', implode($order->getFirstErrors(), '<br/>'));
			return $this->actionPlan();
		}			
	}

    
    
	public function actionOrder($id) {
		$customer = Yii::$app->user->identity->getCustomer();
        
		$plan = $this->findPlanById($id);
		$order = new Orders();
				
		// insert into Orders		
		$order->OrderDate = new \yii\db\Expression('NOW()');
		$order->CustomerID = Yii::$app->user->identity->CustomerID;
		$order->PlanID = $plan->PlanID;
		$order->Period = 1;
		$order->Amount = $plan->Price;
		$order->Address = $customer->Address;
		$order->CompanyName = $customer->CompanyName;		 
		$order->PaymentMethodID = PAYMENT_METHOD_CLICKBANK;
		$order->OrderStatusID = ORDER_PENDING;
				
		if ($order->save()) {
            return $this->redirect('http://' . $plan->FastSpringProduct . '.listen6.pay.clickbank.net?email=' . Yii::$app->user->identity->Email . '&custom1=' . $order->getPrimaryKey());
		} else {
			Yii::$app->getSession()->setFlash('error', implode($order->getFirstErrors(), '<br/>'));
			return $this->actionPlan();
		}			
	}
	
	

    function getRequest($name) {
        return (isset($_REQUEST[$name]) ? $_REQUEST[$name] : '');
    }
    
    
    function cbValid(){
        $key=\Yii::$app->params['CB_SITE_SECRET_KEY'];
        $rcpt=$this->getRequest('cbreceipt');
        $time=$this->getRequest('time');
        $item=$this->getRequest('item');
        $cbpop=$this->getRequest('cbpop');

        $xxpop=sha1($key."|".$rcpt."|".$time."|".$item);
        $xxpop=strtoupper(substr($xxpop,0,8));
        
        if ($cbpop==$xxpop){
            return 1;
        }
        else {
            return 0;
        }
    }
    
    
    
    public function actionThankyou() {
        if (\Yii::$app->user->isGuest) {
            $this->layout = '//main-login';
        }
        
        if (!$this->cbValid()) {
            $this->error('Clickbank error: ' . var_export($_REQUEST, true));
            return $this->render('error');
        }

        return $this->render('thankyou');
    }
    
    
    
	public function actionUpgrade($id) {

		$customer = Yii::$app->user->identity->getCustomer();
        
        $planTrial = $this->findPlanBySlug('trial');
        if ($customer->PlanID == $planTrial->PlanID)
        {
            return $this->actionOrder($id);
        }
        
        $plan = $this->findPlanById($id);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.clickbank.com/rest/1.3/orders2/".$customer->FastSpringRef."/changeProduct");
        curl_setopt($ch, CURLOPT_HEADER, true);                 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);                
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/xml", "Authorization: ".CB_DEV_KEY.":".CB_CLERK_KEY));

        $payload = array();
        $payload['newSku'] = $plan->slug;
        $payload['oldSku'] = $customer->getPlan()->slug;
        $payload['carryAffiliate'] = 1;
        $payload = http_build_query($payload);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        $result = curl_exec($ch);
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($errno) {
            $this->error($error . ":" .$errno . "| ");
            $this->error($response);
            Yii::$app->end();
        }

        $customer->PlanID = $plan->PlanID;
        $customer->PlanID = $plan->PlanID;
        $customer->save();

        return $this->actionPlan();
	}


	
	public function actionActivatex()
    {
        // NOTE: the mcrypt libraries need to be installed and listed as an
        // available extension in your phpinfo() to be able to use this
        // method of decryption.

        $secretKey = \Yii::$app->params['CB_SITE_SECRET_KEY']; // secret key from your ClickBank account

        // get JSON from raw body...
        $message = json_decode(file_get_contents('php://input'));

        // Pull out the encrypted notification and the initialization vector for
        // AES/CBC/PKCS5Padding decryption
        $encrypted = $message->{'notification'};
        $iv = $message->{'iv'};
        $this->trace("IV: " . $iv);

        // decrypt the body...
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128,
        substr(sha1($secretKey), 0, 32),
        base64_decode($encrypted),
        MCRYPT_MODE_CBC,
        base64_decode($iv)), "\0..\32");
        $this->trace("Decrypted: ". $decrypted);

        ////UTF8 Encoding, remove escape back slashes, and convert the decrypted string to a JSON object...
        $sanitizedData = utf8_encode(stripslashes($decrypted));
        $payment = json_decode($decrypted, true);
        
        $this->trace(var_export($payment, true));
        
        // first order id
        $parts = parse_url($payment['lineItems'][0]['downloadUrl']);
        $query = '';
        parse_str($parts['query'], $query);
        $orderBaseId = $query['custom1'];

        $baseOrder = Orders::findOne($orderBaseId);
        
        if ($baseOrder == null) {
            // create customer
            $password = $this->randomPassword();

            $signup = new SignupForm();
            $signup->terms = 1;
            $signup->email = $payment['customer']['billing']['email'];
            $signup->password = $password;

            $user = $signup->signup();

            $customer = Customers::findOne(['CustomerID' => $user->CustomerID]);
            $customer->Active = 1;
            $customer->PlanActive = 1;
            $customer->save();

            // send email
            \Yii::$app->mail->compose('welcomeEmailSocial', ['user' => $user, 'password' => $signup->password])
                    ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
                    ->setTo($user->Email)
                    ->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Welcome'))
                    ->send();            
        }
        
        $subscription_ref = $payment['receipt'];                
        if ($payment['transactionType'] == 'SALE' ||
                $payment['transactionType'] == 'BILL'||
                $payment['transactionType'] == 'TEST'||
                $payment['transactionType'] == 'TEST_BILL'||
                $payment['transactionType'] == 'TEST_SALE') 
        {    
            
            // create the order if not exists
            $order = Orders::findOne(['PaymentMethodID' => PAYMENT_METHOD_CLICKBANK, 'Details' => $subscription_ref]);
            if ($order == null)
            {                
                if (!$baseOrder->Details) {
                    $order = $baseOrder;
                } else {
                    $order = new Orders();
                }
                
                $plan = $this->findPlanByFastSpringId($payment['lineItems'][0]['itemNo']);
                                                
                $order->OrderDate = new \yii\db\Expression('NOW()');
                $order->CustomerID = $baseOrder->CustomerID;
                $order->PlanID = $plan->PlanID;
                $order->Period = 1;
                $order->Amount = $plan->Price;
                $order->Address = $payment['customer']['billing']['email'];
                $order->CompanyName = $payment['customer']['billing']['fullName'];		 
                $order->PaymentMethodID = PAYMENT_METHOD_CLICKBANK;
                $order->OrderStatusID = ORDER_PENDING;
            }
            
            if ($order->save()) {
                $this->recordPaidOrder($order->getPrimaryKey(), $subscription_ref);
            }			            
            
            // initial sale, save receipt
            if ($payment['transactionType'] == 'SALE' ||
                $payment['transactionType'] == 'TEST_SALE')
            {
                $connection = \Yii::$app->db;		                
                $connection	->createCommand()
                            ->update('customers', 
                                      [
                                        'FastSpringRef' => $subscription_ref,
                                        'PaymentMethodID' => PAYMENT_METHOD_CLICKBANK  
                                      ],
                                      'CustomerID = ' . $baseOrder->CustomerID
                                    )
                            ->execute();
            }
        }
        else if ($payment['transactionType'] == 'CANCEL-REBILL'||
                $payment['transactionType'] == 'CGBK'||
                $payment['transactionType'] == 'RFND'||
                $payment['transactionType'] == 'INSF'||
                $payment['transactionType'] == 'TEST_RFND'||
                $payment['transactionType'] == 'CANCEL-TEST-REBILL')
        {
            $order = Orders::findOne(['PaymentMethodID' => PAYMENT_METHOD_CLICKBANK, 'Details' => $subscription_ref]);
            $order->OrderStatusID = ORDER_CANCELED;
            $order->save();
                        
            $connection = \Yii::$app->db;
            $connection	->createCommand()
                        ->update('customers', 
                                  [
                                    'PlanActive' => 0,
                                    'PlanExpirationTime' => new \yii\db\Expression('sysdate()'),
                                  ],
                                  'CustomerID = ' . $baseOrder->CustomerID
                                )
                        ->execute();            
        }        
    }		
	
	
		
    protected function findOrder($id)
    {
        if (($model = Orders::findOne(['OrderID'=>$id, 'CustomerID'=>Yii::$app->user->identity->CustomerID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }	

    protected function findPlanBySlug($slug)
    {
        if (($model = Plans::findOne(['slug' => $slug])) !== null) {
            return $model;
        } else {
            return null;
        }
    }	
		
    protected function findPlanByFastSpringId($slug)
    {
        if (($model = Plans::findOne(['FastSpringProduct' => $slug])) !== null) {
            return $model;
        } else {
            return null;
        }
    }	

    protected function findPlanById($id)
    {
        if (($model = Plans::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }		
}


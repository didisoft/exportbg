<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\controllers\MyController;
use common\models\Customers;
use common\models\Logins;
use common\models\Sources;
use common\models\MentionsJobs;
use common\models\Words;
use frontend\models\ASearchResult;


class AController extends MyController
{
	public $orTerms = null;
	public $exactTerms = null;
	public $excludeTerms = null; // e.g. "\"C-24\" самолет"
	public $lang = null;
}
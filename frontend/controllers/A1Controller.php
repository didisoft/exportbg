<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\controllers\MyController;
use common\models\Customers;
use common\models\Logins;
use common\models\Sources;
use common\models\MentionsJobs;
use common\models\Words;
use frontend\models\ASearchResult;


class A1Controller extends AController
{
	public $service = null;
	public $client = null;

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 *
	 * URL: /a1/search?word=
	 */
	public function actionSearch($word, $orTerms, $exactTerms, $excludeTerms, $lang, $first=0)
	{
		$this->orTerms = $orTerms;
		$this->exactTerms = $exactTerms;
		$this->excludeTerms = $excludeTerms;
		$this->lang = $lang;

		$this->client = new \Google_Client();//Client();
		$this->client->setApplicationName(\Yii::$app->params['google_app_name']);
		$this->client->setDeveloperKey(\Yii::$app->params['google_api_key']);
		$this->service = new \Google_Service_Customsearch($this->client);
		
		$days = 1;
		$results = $this->Search($this->service, $word, $days, $first);

		$newResuts = array();
		foreach ($results as $result) 
		{
			$newResut = new ASearchResult();
			$newResut->sourceId = $this->GetSourceId();
			$newResut->title = $result->getTitle();
			$newResut->url = $result->getLink();
			$newResut->snippet = $result->getSnippet();
			$newResuts[] = $newResut;
		}
		
		return $newResuts;		
	}
	
	function GetSourceId() {
		return JOB_ID_Google;
	}
	
	function GetName() {
		return "Google";
	}
	
	function SearchTest() {
		$out = "<h3>Results for pgp</h3>";
		$results = $this->GetResultsFor($this->service, 'pgp', 1);
		$out .= $this->FormatResults($results);
		echo $out;
	}	

	function Search($service, $word, $days=1, $isFirtsSearch = false) 
	{ 
		/************************************************
		  We make a call to our service, which will
		  normally map to the structure of the API.
		  In this case $service is Books API, the
		  resource is volumes, and the method is
		  listVolumes. We pass it a required parameters
		  (the query), and an array of named optional
		  parameters.
		 ************************************************/
		 // Google_Service_Customsearch_Search
		// Google_Service_Customsearch_Cse_Resource
		$cse = $service->cse; 

		// https://developers.google.com/custom-search/docs/xml_results#num
		$params = array('cx'=>\Yii::$app->params['google_cse_cx_id'],
				//'num' => '20',
				// d[number]: requests results from the specified number of past days.
				// w[number]: requests results from the specified number of past weeks.
				// m[number]: requests results from the specified number of past months.
				// y[number]: requests results from the specified number of past years.
				'dateRestrict'=>'d'. $days,
				'lr' => 'lang_' . \Yii::$app->language); // this must be a setting

		if (!empty($this->lang)) {
			$params['lr'] = $this->lang; // override for my own use only
		}		
		if (!empty($this->orTerms)) {
			$params['orTerms'] = $this->orTerms;
		}		
		if (!empty($this->exactTerms)) {
			$params['exactTerms'] = $this->exactTerms;
		}		
		if (!empty($this->excludeTerms)) {
			$params['excludeTerms'] = $this->excludeTerms;
		}		
		
		$this->trace($word);
		$this->trace(var_export($params, true));
		
		$list = $cse->listCse($word, 
							  $params);
		
		// array Google_Service_Customsearch_Result
		$results = $list->getItems();
			
		// Google_Service_Customsearch_SearchSearchInformation
		$info = $list->getSearchInformation();
		$totalResults = $info->getTotalResults();
		
		$resultsPerPage = 10;		
		$pagesCount = (int)$totalResults / $resultsPerPage;
		if (($totalResults % $resultsPerPage) > 0) {
			$pagesCount++;
		}
		
		$additionalPages = 2;
		if ($isFirtsSearch) 
		{
			$additionalPages = 5;
		}
		
		if ($pagesCount > $additionalPages) 
		 for ($i = 1; $i < $additionalPages; $i++) {
			$params = array_merge($params, array('start' => $i * $resultsPerPage + 1));
			$list = $cse->listCse($word, 
								  $params);
								  
			$results = array_merge( $results, $list->getItems() );
		 }			
		
			
		// array Google_Service_Customsearch_Result
		return $results;
	}	
	
	function FormatResults($results)
	{
		$out = '';
		foreach ($results as $item) 
		{
			$out .= '<br>';	
			$out .= '<b>' .$item->getTitle() . '</b>';
			$out .= '<br>';
			$out .= '<a href="' . $item->getLink() . '">' .$item->getLink() . '</a>';
			$out .= '<p>' . $item->getSnippet() . '</p>';
		}
		return $out;
	}			
}
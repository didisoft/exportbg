<?php
namespace frontend\controllers;

use Yii;
use common\models\Words;
use common\models\LoginForm;
use common\models\Customers;
use common\models\User;
use common\controllers\MyController;
use common\models\AuthConnect;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
/**
 * Site controller
 */
class SiteController extends MyController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
			'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'socialLoginCallback'],
            ],			
			'reg' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'socialRegisterCallback'],
            ],			
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	
	
    public function actionIndex()
    {
		if (Yii::$app->user->isGuest) {
			return $this->actionLogin();
		}
		
		$customer = Customers::findOne(Yii::$app->user->identity->CustomerID);	
		$session = \Yii::$app->session;
		$session->set('Active', $customer['Active']);
		
		$wordsCount = Words::find()->where(['CustomerID'=>Yii::$app->user->identity->CustomerID])
									->count();  
	
		if ($wordsCount > 0) {
			return Yii::$app->runAction('mentions/index');
		} else {
			return $this->render('indexempty');
		}
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {						
            return $this->goBack();
        } else {
            $this->layout = '//main-login';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	
	
	/**
	* Creates a new customer.
	* The customer registration logic is in SignupForm::signup()
	*/
    public function actionSignup()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }        
        
		$this->layout = 'main_login';
		
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) 
			{
                \Yii::$app->mail->compose('activationEmail', 
												['user' => $user, 'password' => $model->password])
                    ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
                    ->setTo($user->Email)
                    ->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Account created'))
                    ->send();

				Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Check your email for further instructions.'));
                
                $this->layout = '//main-login';
                return $this->render('activation-notes');
            }
        }

        $this->layout = '//main-login';
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Check your email for further instructions.'));

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        $this->layout = '//main-login';
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Data saved'));

            return $this->goHome();
        }

        $this->layout = '//main-login';
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
    public function actionActivate($id, $token)
    {
        if (empty($token) || !is_string($token)) {
			return $this->render('activate', [
				'activated' => false,
			]);
        }
        $user = User::findIdentity($id);
        if (!$user) {
			return $this->render('activate', [
				'activated' => false,
			]);
        }
		
		if ($user->validateAuthKey($token)) 
		{
			$customer = Customers::findOne($user->CustomerID);				
			$customer->Active = 1;
			$customer->save();
			// send email
			\Yii::$app->mail->compose('welcomeEmail', 
											['user' => $user])
				->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
				->setTo($user->Email)
				->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Welcome'))
				->send();
			
				if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                } else {
					throw new InvalidParamException('User cannot login');				
				}				
		} else {
			return $this->render('activate', [
				'activated' => false,
			]);		
		}
    }	
	
	
    
    // https://github.com/yiisoft/yii2-authclient/blob/master/docs/guide/quick-start.md
    public function socialLoginCallback($client) {
        
        $email = $this->getAttrEmail($client);
        $user = User::findOne(['Email'=>$email]);
        if (!empty($user)) {
            Yii::$app->user->login($user);
        } else {
            $found = AuthConnect::findOne(['Email' => $email]);
            if ($found) {
                $user = \common\models\User::findOne(['LoginID'=>$found->LoginID]);
                Yii::$app->user->login($user);                
            } 
            else
            {
                Yii::$app->getSession()->setFlash('error', [
                                    Yii::t('app',
                                        'Unable to find account for {email}. Would you like to Register?',
                                        ['email' => $email]),
                                ]);                    
            }
        }        
    }



    public function socialRegisterCallback($client) {
        $attributes = $client->getUserAttributes();
        $nickname = '';
        if ($client->getName() == 'google') {
            $nickname = $attributes['displayName'];
        } else if ($client->getName() == 'facebook') {
            $nickname = $attributes['first_name'];
        }
        $email = $this->getAttrEmail($client);
        if (User::findOne(['Email'=>$email])) {
            $this->socialLoginCallback($client);
            return;
        } else if (AuthConnect::findOne(['Email' => $email])) {
            $this->socialLoginCallback($client);
            return;
        }
        
		$password = $this->randomPassword();
				
        $signup = new SignupForm();
        $signup->terms = 1;
        $signup->email = $email;
        $signup->password = $password;
        
        $user = $signup->signup();
        
        $customer = Customers::findOne(['CustomerID' => $user->CustomerID]);
        $customer->Active = 1;
        $customer->PlanActive = 1;
        $customer->save();
        
        // send email
        \Yii::$app->mail->compose('welcomeEmailSocial', ['user' => $user, 'password' => $signup->password])
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['siteName']])
                ->setTo($user->Email)
                ->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Welcome'))
                ->send();
        
        \Yii::$app->user->login($user);
    }	
		
}

<?php
namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
	
use common\controllers\MyController;
use common\models\Plans;
use common\models\Orders;
use common\models\Words;
use common\models\Invoices;

defined('LIVE') || define('LIVE', strpos($_SERVER['HTTP_HOST'],'localhost')===false ? false/*true*/ : false);
if (LIVE) {
  define('EPAY_URL','https://www.epay.bg/');
  define('EPAY_CLIENT_EMAIL','office@ereport.bg');
  define('EPAY_CLIENT_NUMBER','4669434956');
  define('EPAY_SECRET_KEY','RAK40PGSHHWDDLPA4Y58EIMN7RDE08LW8BFY0Y6XGQ0HCNHTZAPRLT9JZQK2KSMV');
}else{
  define('EPAY_URL','https://demo.epay.bg/');
  define('EPAY_CLIENT_EMAIL','office@ereport.bg');
  define('EPAY_CLIENT_NUMBER','D287885118');
  define('EPAY_SECRET_KEY','TMUHJ50S4FS4AHYH63H5TQR064JV0ZZYU4TAPJ7RR65FJB5X7CWB3UXMT7U2YEG8');
}

class EpayController extends MyController
{
	// required for ePay notification
	public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['notify'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }



    /**
     * Lists all Words models.
     * @return mixed
     */
    public function actionIndex()
    {
    }
	
	
	// $type - login, card	
	public function actionPost($type, $invoiceId, $price, $description){
        $dt = strtotime('+1 day');
        $expDate = date('d.m.Y', $dt);
        $min=EPAY_CLIENT_NUMBER;
        $data = <<<DATA
MIN={$min}
INVOICE={$invoiceId}
AMOUNT={$price}
EXP_TIME={$expDate}
DESCR={$description}
DATA;
 
        # XXX Packet:
        #     (MIN or EMAIL)=     REQUIRED
        #     INVOICE=            REQUIRED
        #     AMOUNT=             REQUIRED
        #     EXP_TIME=           REQUIRED
        #     DESCR=              OPTIONAL
 
        $encoded = base64_encode($data);
		$checksum = $this->hmac('sha1', $encoded, EPAY_SECRET_KEY);
		$this->layout = 'blank';
		if ($type == 'card') {
			return $this->render('card', [
				'encoded'=>$encoded,
				'checksum'=>$checksum,
			]);		
		} else {
			return $this->render('form', [
				'encoded'=>$encoded,
				'checksum'=>$checksum,
			]);		
		}
    }
	
	
	
	public function actionNotify(){
        $logCat='epay';
        if(empty($_POST['encoded']) || empty($_POST['checksum'])){
            $this->error('Missing encoded or checksum POST variables');
        }else{
            $encoded = $_POST['encoded'];
            $checksum = $_POST['checksum'];
            $hmac = $this->hmac('sha1', $encoded, EPAY_SECRET_KEY); # XXX SHA-1 algorithm REQUIRED
			
            if ($hmac == $checksum) { # XXX Check if the received CHECKSUM is OK
                $data = base64_decode($encoded);
                $lines_arr = preg_split("/[\n,]+/", $data);//split("\n", $data)
                $infoData = '';
                foreach ($lines_arr as $line) {
                    if (preg_match("/^INVOICE=(\d+):STATUS=(PAID|DENIED|EXPIRED)(:PAY_TIME=(\d+):STAN=(\d+):BCODE=([0-9a-zA-Z]+))?$/",
                            $line, $regs)) {
						
						$this->trace('ePay.bg Response :'.$line);	
							
                        $invoice = $regs[1]; // order id
                        $status = $regs[2];
						$infoData .= "INVOICE=$invoice:STATUS=OK\n";
						if($status==='PAID'){
							$payDate = $regs[4]; # YYYYMMDDHHIISS
							$stan = $regs[5]; # XXX if PAID
							$bcode = $regs[6]; # XXX if PAID
							# XXX process $invoice, $status, $payDate, $stan, $bcode here
							# XXX if OK for this invoice														
							$this->recordPaidOrder($invoice, $line);
						} else {
							$this->error('Bad order status: ' . $status);	
						}	
                    }
                }
				
				$this->trace('Our Response:'.$infoData);				
                echo $infoData, "\n";
            }
            else {				
                echo "ERR=Not valid CHECKSUM\n";
                $this->error('ERR=Not valid CHECKSUM');
            }
        }
    }


	
	private function hmac($algo,$data,$passwd){
        /* md5 and sha1 only */
        $algo=strtolower($algo);
        $p=array('md5'=>'H32','sha1'=>'H40');
        if(strlen($passwd)>64)
            $passwd=pack($p[$algo],$algo($passwd));
        if(strlen($passwd)<64)
            $passwd=str_pad($passwd,64,chr(0));
 
        $ipad=substr($passwd,0,64) ^ str_repeat(chr(0x36),64);
        $opad=substr($passwd,0,64) ^ str_repeat(chr(0x5C),64);
        return($algo($opad.pack($p[$algo],$algo($ipad.$data))));
    }	
}

<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\controllers\MyController;
use common\models\Customers;
use common\models\Logins;
use common\models\Sources;
use common\models\MentionsJobs;
use common\models\MentionsPipe;
use common\models\Words;
use frontend\models\Mentions;

use frontend\components\AGoogleSearch;

// 9.6 -  
// 3.5 - 
//use Google\*;
//use Google\Service\*;
//Yii::import('application.components.Google.*');
//Yii::import('application.components.Google.Service.*');

class SearchController extends MyController
{	
    public function actionSearchNew() {
        $maxRuns = 4;
        
        $jobs = MentionsJobs::findAll(['JobStatus' => JOB_Status_Pending]);
        $i = 0;
        foreach ($jobs as $job)
        {
            $word = Words::findOne(['WordID' => $job->WordID]);
            $sourceId = $job->SourceID;
            echo 'Searching for word : ' . $word->WordText;
			$this->trace('Searching for word : ' . $word->WordText);
            $results = Yii::$app->runAction('a'.$sourceId.'/search', 
                                                ['word' => $word->WordText, 
                                                'excludeTerms' => $word->ExcludeTerms,
                                                'orTerms' => $word->OrTerms,
                                                'exactTerms' => (($word->ExactMatch > 0) ? $word->WordText : null),
                                                'lang' => $word->Lang,
                                                'first' => false]);				
            
			// save results
            $mentions = [];
            $expression = new \yii\db\Expression('NOW()');
            $date = (new \yii\db\Query)->select($expression)->scalar();
			foreach ($results as $result) {
				$this->trace('Saving result : ' . $result->title);
				$mention = new Mentions();
				$mention->MentionDate = $date;
				$mention->CustomerID = $job->CustomerID;
				$mention->SourceID = $sourceId;
				$mention->WordID = $word->WordID;
				$mention->JobID = $job->getPrimaryKey();
				$mention->Title = empty($result->title) ? '-' : $result->title;
				$mention->Link = $result->url;
				$mention->Snippet = $result->snippet;
				if (!$mention->validate() || !$mention->save(false)) {
					$this->error( var_export($mention->getErrors(), true) );
				}				
                
                $mentions[] = $mention;
			}
			
			$job->MentionsCount = count($results);
			$job->JobStatus = JOB_Status_Finished;
			$job->JobEndTime = new \yii\db\Expression('NOW()');
			$job->save();
            
            // send $mentions 
            if (count($mentions) > 0) {
                $logins = Logins::findAll(['CustomerID' => $job->CustomerID]);
                foreach ($logins as $login) {
                    foreach($mentions as $mention) {
                        $pipe = new MentionsPipe();
                        $pipe->LoginID = $login->LoginID;
                        $pipe->MentionID = $mention->getPrimaryKey();
                        $pipe->WordID = $mention->WordID;
                        $pipe->save(false);
                    }
                }
            }
            
            $i++;
            if ($i >= $maxRuns)                
                break;
        }
    }
    
    
    
    private function sendMentions($customerId, $word, $mentions) {
        $resultsTable = $this->renderPartial('results', array('word'=>$word->WordText, 'results'=>$mentions), true);						

        $logins = Logins::findAll(['CustomerID' => $customerId]);
        foreach ($logins as $login) {
                $email = empty($login->EmailForMentions) ? $login->Email : $login->EmailForMentions;
				\Yii::$app->mail->compose('resultsEmail', array('word'=>$word->WordText, 
																	'date'=>date('Y-m-d'),
																	'results'=>$resultsTable))
					->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['adminName']])
					->setTo($email)
					->setSubject('['.Yii::$app->name.'] ' . Yii::t('app', 'Alert for') . ' [' . $word->WordText . ']')
					->send();			            
        }
    }
    
    
    
	// Calling as /search/jobrunat?id=24
	public function actionJobrunat($id) {
		if ($id != 24) {
			$this->error('Search Job invoked with wrong key!');
			return;
		}

		// for each Active Customer, for each Active Source
		// check where LastJobStartTime > 24h and select
		// for each Word - run search with that source

		$tuple = $this->GetNextCustomerAndSource();
		if (count($tuple) == 0) {
			$this->trace('No customer for processing');
			return Yii::$app->runAction('cron/cron');
		}		
		$customerId = $tuple[0];
		$sourceId = $tuple[1];
		$isFirst = (bool)$tuple[2];
		
		$this->trace('Customer ID ' . $customerId . ' Source ID ' . $sourceId);
		
		$customer = Customers::findOne($customerId);		

		$words = Words::find(['Active' => 1, 'CustomerID' => $customerId])->orderBy('WordID')->limit($customer->plan->WordsCount)->all();
		$jobs = [];	
		foreach ($words as $word) 
		{	
			$job = new MentionsJobs();
			$jobs[] = $job;
			
			$job->CustomerID = $customerId;
			$job->JobStatus = JOB_Status_Started;
			$job->WordID = $word->WordID;
			$job->WordGroupID = $word->WordGroupID;
			$job->SourceID = $sourceId;
			$job->JobStartTime = new \yii\db\Expression('NOW()');
			$job->JobEndTime = new \yii\db\Expression('NULL');
			if (!$job->save(false)) {
				$this->error( var_export($job->getErrors(), true) );
			}				
			
			$this->trace('Searching for word : ' . $word->WordText);
			try {			
				$results = Yii::$app->runAction('a'.$sourceId.'/search', 
													['word' => $word->WordText, 
													'excludeTerms' => $word->ExcludeTerms,
													'orTerms' => $word->OrTerms,
													'exactTerms' => (($word->ExactMatch > 0) ? $word->WordText : null),
													'lang' => $word->Lang,
													'first' => $isFirst]);				
			} catch (\Exception $e) {
				$this->trace('Deleting Job ID ' . $job->JobID);
				foreach ($jobs as $job) {
					$job->delete(true);
				}
				$this->trace('Caught exception: ');
				$this->trace($e);
				exit;
			}
			
			// save results
			foreach ($results as $result) {
				$this->trace('Saving result : ' . $result->title);
				$mention = new Mentions();
				$mention->MentionDate = new \yii\db\Expression('NOW()');
				$mention->CustomerID = $customerId;
				$mention->SourceID = $sourceId;
				$mention->WordID = $word->WordID;
				$mention->JobID = $job->getPrimaryKey();
				$mention->Title = empty($result->title) ? '-' : $result->title;
				$mention->Link = $result->url;
				$mention->Snippet = $result->snippet;
				if (!$mention->validate() || !$mention->save(false)) {
					$this->error( var_export($mention->getErrors(), true) );
				}				
			}
			
			$job->MentionsCount = count($results);
			$job->JobStatus = JOB_Status_Finished;
			$job->JobEndTime = new \yii\db\Expression('NOW()');
			$job->save();
		} // foreach $words

		return '';
	}
	

	
	// Selection Algorithm
	// Result [customerId, sourceId, isFirstSearch]
	// -------------------
	// for each Active Customer
	// for search Source	
	// if there is no execution at all 
	//	return it
	function GetNextCustomerAndSource() {

		$connection = \Yii::$app->db;
	
		$sources = Sources::findAll(['Active'=>1]);			
		foreach ($sources as $source) {
			$sql = 'select *
					from customers c
					where c.PlanActive = 1
					and exists (select * from words w where c.CustomerID = w.CustomerID)
					and not exists (select * from mentionsjobs m where c.CustomerID = m.CustomerID and m.SourceID = '.$source->SourceID.')';		
					
			$model = $connection->createCommand($sql);
			$customers = $model->queryAll();			
			foreach ($customers as $customer) {			
						return [$customer['CustomerID'], 
									$source->SourceID,
									true];			
			}					
		}
	
		// run subsequent reports
		$sources = Sources::findAll(['Active'=>1]);
		foreach ($sources as $source) {
		
			$sql = 'select *
					from customers c
					where c.PlanActive = 1
					and exists (select * from words w where c.CustomerID = w.CustomerID and w.Active = 1)
					and not exists (select * from mentionsjobs m where c.CustomerID = m.CustomerID and m.SourceID = '.$source->SourceID.'
									and m.JobStartTime > (NOW() - INTERVAL 12 HOUR))';
					
			$model = $connection->createCommand($sql);
			$customers = $model->queryAll();
			
			foreach ($customers as $customer) {								
				return [$customer['CustomerID'], 
								$source->SourceID,
								false];			
								
			}
		}
		
		return array();	
	}	
		
}
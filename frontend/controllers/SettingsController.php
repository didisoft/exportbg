<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;

use common\models\AuthConnect;

class SettingsController extends \common\controllers\MyController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
			'connect' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'socialConnectCallback'],
            ]
        ];
    }

    public function actionIndex()
    {
		$model = Yii::$app->user->identity;
		$customer = Yii::$app->user->identity->getCustomer();
		$plan = $customer->getPlan();
		
        return $this->render('index', [
            'model' => $model,
			'customer' => $customer,
			'plan' => $plan,
        ]);
    }	

	/** in-place diting */
    public function actionEditmailpreferences()
    {
		// Check if there is an Editable ajax request
		if (isset($_POST['hasEditable'])) {
			$model = Yii::$app->user->identity;
			// read your posted model attributes			
			$model->attributes = $_POST['User'];
			// read or convert your posted information
			if ($model->save(false)) {				
				$postValues = $_POST[$model->formName()];
				foreach ($postValues as $name => $val) {
					$value = $model->$name;
				}

				// return JSON encoded output in the below format
				echo \yii\helpers\Json::encode(['output'=>$value, 'message'=>'']);
			} else {
			// alternatively you can return a validation error
				echo \yii\helpers\Json::encode(['output'=>'', 'message'=>'Validation error']);
			}
			return;
		}
    }
	
    public function actionSettings()
    {
		$model = Yii::$app->user->identity;
		
		if (Yii::$app->request->post('User') != null) {
			$user = Yii::$app->request->post('User');
			$model->FirstName = $user['FirstName'];
			$model->LastName = $user['LastName'];
			$model->Email = $user['Email'];
			if ($model->save()) {	
				Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data saved') );
			} else {
				Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error saving data') );
			}
		}
		
        return $this->render('settings', [
            'model' => $model,
        ]);
    }	


	
    public function actionPassword()
    {
		if (Yii::$app->request->isPost) {
			if (!Yii::$app->user->identity->validatePassword(Yii::$app->request->post('old_password'))) {
				Yii::$app->getSession()->setFlash('error', 'Старата парола е грешна!');			
			}
			else if (!Yii::$app->request->post('new_password') ||
				!Yii::$app->request->post('repeat_password')) {
				Yii::$app->getSession()->setFlash('error', 'Новата парола не може да е празна!');			
			} 
			else if (Yii::$app->request->post('repeat_password') != Yii::$app->request->post('new_password')) {
				Yii::$app->getSession()->setFlash('error', 'Новата парола не съвпада с повтореното въвеждане!');	
			}			
			else if (mb_strlen(Yii::$app->request->post('new_password')) < 4 ) {
				Yii::$app->getSession()->setFlash('error', 'Новата парола не може да е по къса от 4 синвола!');			
			} 
			else {
				Yii::$app->user->identity->setPassword(Yii::$app->request->post('new_password'));				
			}
		}
		
        return $this->render('password');
    }	



    public function actionCompany()
    {
		$model = Yii::$app->user->identity->getCustomer();
		
		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				Yii::$app->getSession()->setFlash('success', 'Data saved');
				return $this->actionIndex();
			} else {
				Yii::$app->getSession()->setFlash('error', 'Error saving data.');
			}
		}
		
        return $this->render('company', [
            'model' => $model,
        ]);
    }	        
    
    
    public function socialConnectCallback($client) {
        $email = $this->getAttrEmail($client);
        
        $found = AuthConnect::findOne(['Email' => $email]);
        if ($found === null) 
        {
            $connect = new AuthConnect();
            $connect->LoginID = Yii::$app->user->identity->LoginID;
            $connect->Email = $email;
            $connect->save(false);
        }
        
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Social account connected with {client}', ['client' => $client->getTitle()]));    
        
        return $this->action->redirect( \yii\helpers\Url::to(['settings/index'], true) );
    }	            
}
